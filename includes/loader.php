<?php
/**
 * Main theme loader file
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

if ( ! class_exists( 'SH_Base' ) ) {
	include_once get_template_directory() . '/includes/base.php';
}

add_action( 'init', 'sh_theme_init' );


if ( ! function_exists( 'sh_set' ) ) {

	/**
	 * [sh_set description]
	 *
	 * @param  array  $var [description].
	 * @param  string $key [description].
	 * @param  string $def [description].
	 * @return [type]      [description]
	 */
	function sh_set( $var, $key, $def = '' ) {

		if ( is_object( $var ) && isset( $var->$key ) ) {
			return $var->$key;
		} else if ( is_array( $var ) && isset( $var[ $key ] ) ) {
			return $var[ $key ];
		} else if ( $def ) {
			return $def;
		} else {
			return false;
		}
	}
}


if ( ! function_exists( 'printr' ) ) {

	/**
	 * [printr description]
	 *
	 * @param  array $data [description].
	 * @return void       [description]
	 */
	function printr( $data ) {
		echo '<pre>';
		print_r( $data );
		exit;
	}
}

if ( ! function_exists( '_font_awesome' ) ) {

	/**
	 * [_font_awesome description]
	 *
	 * @param  string $index [description].
	 * @return [type]        [description]
	 */
	function _font_awesome( $index ) {

		$array = array_values( $GLOBALS['_font_awesome'] );

		if ( $font = sh_set( $array, $index ) ) {
			return $font;
		} else {
			return false;
		}

	}
}


if ( ! function_exists( '_load_class' ) ) {

	/**
	 * [_load_class description]
	 *
	 * @param  string  $class     [description].
	 * @param  string  $directory [description].
	 * @param  boolean $global    [description].
	 * @param  string  $prefix    [description].
	 * @return void             [description]
	 */
	function _load_class( $class, $directory = 'libraries', $global = true, $prefix = 'SH_' ) {

		$obj = &$GLOBALS['_sh_base'];
		$obj = is_object( $obj ) ? $obj : new stdClass;

		$name = false;

		// Is the request a class extension?  If so we load it too.
		$path = get_template_directory().'/includes/'.$directory.'/'.$class.'.php';

		if ( file_exists( $path ) ) {

			$name = $prefix.ucwords( $class );

			if ( false === class_exists( $name ) ) {
				require( $path );
			}
		}

		// Did we find the class?
		if ( false === $name ) {
			exit( 'Unable to locate the specified class in theme: '. esc_attr( $class ).'.php' );
		}

		if ( $global ) {
			$GLOBALS['_sh_base']->$class = new $name();
		} else {
			new $name();
		}
	}
}


include_once get_template_directory() . '/includes/library/form_helper.php';
include_once get_template_directory() . '/includes/library/functions.php';
include_once get_template_directory() . '/includes/library/widgets.php';
include_once get_template_directory() . '/includes/library/hooks.php';


_load_class( 'enqueue', 'helpers', false );

_load_class( 'seo', 'helpers', false );

_load_class( 'bootstrap_walker', 'helpers', false );

_load_class( 'dynamic_styles', 'helpers', false );

if ( is_admin() ) {
	/** Plugin Activation */
	require_once( 'thirdparty'.DIRECTORY_SEPARATOR.'tgm-plugin-activation'.DIRECTORY_SEPARATOR.'plugins.php' );
}


if ( is_admin() ) {

	/** Plugin Activation */
	require_once( get_template_directory().'/includes/thirdparty'. DIRECTORY_SEPARATOR .'tgm-plugin-activation'.DIRECTORY_SEPARATOR.'plugins.php' );
}
/**
 * [sh_theme_init description]
 *
 * @return [type] [description]
 */
function sh_theme_init() {

	global $pagenow;

	return;
}
