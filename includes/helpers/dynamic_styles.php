<?php

/**

 * Dynamic style file

 *

 * @package Comre

 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>

 * @version 1.0

 */



if ( ! defined( 'ABSPATH' ) ) {

	die( 'Restricted' );

}



/**

 * Dynamic style class

 */

class SH_Dynamic_styles

{

	/**

	 * Increment variable

	 * @var increment;

	 *

	 */

	var $increment = 10;



	/**

	 * Data variable
	 *
	 * @var data;
	 */
	var $data;

	/**
	 * Main construcdtor
	 */
	function __construct() {

		require_once get_template_directory(). '/includes/thirdparty/DotNotation.php';

		$this->init();

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );


	}



	/**

	 * [init function]

	 */

	function init() {

		$options = _WSH()->option();
		$options = ( $options ) ? $options : array();

		$dnt = new Comre_DotNotation( $options );
		//print_r($options); exit();
//print_r($dnt->get( 'topbar_bg_color', '#000' )); exit();
//print_r($dnt->get( 'h1_font_family_custom_custom.color', '#000' ) ); exit();

		$config = array(


				'@topbar_height' => $dnt->get( 'topbar_height', '50' ),
				
				'@topbar_bg_color' => $dnt->get( 'topbar_bg_color.color', '#fff' ),

				'@topbar_text_color' => $dnt->get( 'topbar_text_color.color', '#222222'  ),
				
				'@topbar_text_transform' =>  $dnt->get( 'topbar_text_color.transform', 'capitalize' ),

				'@topbar_text_size' => $this->set_px( $dnt->get( 'topbar_text_color.size', '14px' ) ),

				'@topbar_text_spacing' => $this->set_px( $dnt->get( 'topbar_text_color.spacing', '' ) ),

				'@topbar_text_height' => $this->set_px( $dnt->get( 'topbar_text_color.height', '' ) ),

				'@topbar_link_color' => $dnt->get( 'topbar_link_color.color', '#fffefe' ),



			    '@topbar_link_bg'	=>  $dnt->get( 'topbar_link_bg.color', '' ),

				'@topbar_icon_bg_color'	=> $dnt->get( 'topbar_icon_bg_color.color', '' ),

				'@topbar_border_color'   	=> $dnt->get( 'topbar_border_color.color', '#dddddd' ),

				'@logo_bg_color'	=>  $dnt->get( 'logo_bg_color.color', '#000'  ),

				'@logo_txet_color'	=>  $dnt->get( 'logo_txet_color.color', ''  ),

				'@menubar_height'	=>  $dnt->get( 'menubar_height', '50px'  ),

				'@menu_bg_color'	        => $dnt->get( 'menu_bg_color.color', '#cf000f' ),

				'@active_menu_bg'	    =>  $dnt->get( 'active_menu_bg.color', '#ffdd00'  ),

				'@active_menu_color'	        =>  $dnt->get( 'active_menu_color.color', '#000'  ),

				'@nav_menu_color'	    => $dnt->get( 'menu_custom_typography_options.color', '#fff' ),

			

				'@nav_menu_size'	    => $this->set_px( $dnt->get( 'menu_custom_typography_options.size', '14px' ) ),

				'@nav_menu_transform'	    => $dnt->get( 'menu_custom_typography_options.transform', 'capitalize'  ),
				
				'@nav_menu_lineheight'	    => $this->set_px( $dnt->get( 'menu_custom_typography_options.height', '' ) ),
				'@nav_menu_family'	    =>  $this->family($dnt->get( 'menu_custom_typography_options.family', '')  ),

				'@nav_submenu_bgcolor'	    =>  $dnt->get( 'submenu_bg_color.color', '#fff'  ),

				'@nav_submenu_family'	    =>  $this->family($dnt->get( 'submenu_color.family', '')),

				'@nav_submenu_size'	    => $this->set_px( $dnt->get( 'submenu_color.size', '14px' ) ),

				'@nav_submenu_color'	    =>  $dnt->get( 'submenu_color.color', '#000' ),


				'@nav_submenu_border_color'	    => $dnt->get( 'submenu_border_color.color', '#000'  ),

				'@nav_submenu_border_bottom_color'	    => $dnt->get( 'submenu_border_bottom.color', '#000' ),

				'@nav_submenu_hover_color'	   =>  $dnt->get( 'submenu_hover_color.color', '#000' ),


				'@footer_top_img'	           =>  $dnt->get( 'footer_top_img.image', ''  ),

				'@footer_top_bgcolor'	       =>  $dnt->get( 'footer_top_img.color', '#fff' ),


				'@footer_text_family'	       => $this->font_variant($dnt->get( 'text_typography_options.family', '' )),
			
				'@footer_text_color'	       =>  $dnt->get( 'text_typography_options.color', '#fff' ),

				'@footer_text_varient'	       =>  $dnt->get( 'text_typography_options.varient', '' ),

				'@footer_text_transform'	   => $dnt->get( 'text_typography_options.transform', 'capitalize' 
					),


				'@footer_heading_family'	   => $this->family( $dnt->get('h1_font_family_custom_custom.family', '' ) 
					),
				'@footer_heading_varient'	   => $this->font_variant( $dnt->get( 'h1_font_family_custom_custom.variant', '' ) ),
				'@footer_heading_color'	      => $dnt->get( 'h1_font_family_custom_custom.color', '#000'
					),

				'@footer_heading_size'	     => $this->set_px( $dnt->get( 'h1_font_family_custom_custom.size', '20px' )
					),
				'@footer_heading_transform'	  => $dnt->get( 'h1_font_family_custom_custom.transform', 'capitalize' 
					),




			);



		$this->output( $config );



		add_filter( 'comre_google_fonts_filter', array( $this, 'load_font' ), 10 );

	}



	/**

	 * [function output description]

	 *

	 * @param  string $config  [description].

	 */

	function output( $config ) {



		$content = '';



		ob_start();

		$content = get_template_part( 'includes/resource/dynamic-styles' );



		$content = ob_get_clean();



		$keys = array_keys( $config );

		$values = array_values( $config );



		$final_output = str_replace( $keys, $values, $content );



		$this->data = $final_output;

	}



	/**

	 * [function enqueue description]

	 */

	function enqueue() {

		wp_add_inline_style( 'main_style', $this->data );

	}



	/**

	 * [set_px description]

	 *

	 * @param  string $value  [description].

	 * @param  string $def  [description].

	 */

	function set_px( $value, $def = 0 ) {

		if ( is_numeric( $value ) ) {

			return $value.'px';

		} elseif ( is_numeric( $def ) ) {

			return $def.'px' ;

		} else {

			return 'auto';

		}

	}





	/**

	 * [function font_variant description]

	 *

	 * @param  string $font  [description].

	 * @param  string $type  [description].

	 */

	function font_variant( $font, $type = 'style' ) {



		$vari = sh_set( $font, 'variant' );



		switch ( $vari ) {

			case 'regular':

				 return 300;

				break;



			default:

				return 300;

				break;

		}

	}



	/**

	 * [function font family description]

	 *

	 * @param  string $font  [description].

	 */

	function family( $font ) {

		$family = sh_set( $font, 'family' );



		$this->family = $family;

		$this->increment++;



		$this->fonts_set[ $this->family ] = $this->family;

		return $family;

	}



	/**

	 * [function load_font description]

	 *

	 * @param  string $fonts  [description].

	 */

	function load_font( $fonts ) {



		foreach ( $this->fonts_set as $font ) {

			$fonts[ $font ] = $font . ':400,100,100italic,300,300italic,400italic,700,700italic,900,900italic';

		}

		return $fonts;

	}

}

