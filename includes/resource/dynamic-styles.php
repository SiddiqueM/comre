<?php

$options = _WSH()->option();

$topbar_settings = sh_set( $options, 'show_topbar_typogrphy' );
$header_settings = sh_set( $options, 'show_header_typogrphy' );
$footer_settings = sh_set( $options, 'show_footer_typogrphy' );



?>

<?php if($topbar_settings) : ?>
.top-bar {
	background: @topbar_bg_color;
	border-bottom: 1px solid @topbar_border_color;
	height: @topbar_heightpx;
}
.top-bar ul li a {  
  border-left: 1px solid @topbar_border_color;
}
.top-bar ul li a:hover{
	 background: @topbar_link_bg;
	 color:  @topbar_link_color;
}
.social_icons li:hover a{
	background: @topbar_icon_bg_color;
}
.top-bar li:nth-last-child(1) a {
    border-right: 1px solid @topbar_border_color;
}
.top-bar ul li a{
	color: @topbar_text_color;
	line-height: @topbar_text_height;
	font-size: @topbar_text_size;
	lettter-spacing : @topbar_text_spacing;
	text-transform : @topbar_text_transform;
	color: @topbar_text_color;
}

<?php endif; ?>

<?php if($header_settings) : ?>

header{
	background: @logo_bg_color;
	color: @logo_txet_color;
}
header nav{
	height: @menubar_height;
	background: @menu_bg_color;
}

header nav li.active a, header nav li a:hover, header .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus, .ownmenu > li:hover > a{
	background: @active_menu_bg;
	color: @active_menu_color;	

}
header nav li a {
   color: @nav_menu_color;
   font-size: @nav_menu_size;
   line-height: @nav_menu_lineheight;
   text-transform: @nav_menu_transform;
   font-family: @nav_menu_family;
}
.ownmenu ul.dropdown, .ownmenu ul.dropdown li ul.dropdown{
	background: @nav_submenu_bgcolor;
}
header .ownmenu ul.dropdown li a{
    border-bottom: 1px dashed @nav_submenu_border_color;
    color: @nav_submenu_color;
    font-size: @nav_submenu_size;
    font-family: @nav_submenu_family;
}
 .ownmenu ul.dropdown {
    border-color: @nav_submenu_border_bottom_color;
}
.ownmenu ul.dropdown li:hover > a {
    color: @nav_submenu_hover_color;
}
<?php endif; ?>
<?php if($footer_settings) : ?>
	footer {
	    background: rgba(0, 0, 0, 0) url("@footer_top_img") repeat scroll 0 0;
	    background-color: @footer_top_bgcolor;
	}
	footer .rights {
    	background-color: @footer_top_bgcolor !important;
	}
	footer h6 {
	    color: @footer_heading_color;
	    font-size: @footer_heading_size;
	    text-transform: @footer_heading_transform;
	    font-varient: @footer_heading_varient;
	    font-family : @footer_heading_family;
	}
	footer .links a, footer p {
	    color: @footer_text_color;
		font-family: @footer_text_family;
		font-variant: @footer_heading_varient;
	    font-size: @footer_heading_size;
	    text-transform: @footer_text_transform;

	}

<?php endif; ?>