					
					<!--======= ADMIN INFO =========-->
					<div class="admin-info">
						<h6 class="text-uppercase">
							<?php esc_html_e('About thr author : ', 'comre')?>
							<?php the_author();?>
						</h6>
						<ul class="row">
							<li class="col-xs-2"><?php echo get_avatar('',80);?></li>
							<li class="col-xs-10">
								<p>
									<?php the_author_meta( 'description', get_the_author_meta('ID') ); ?>
								</p>
							</li>
						</ul>
						
						<!--======= SOCIAL ICONS =========-->
						<ul class="social_icons">
							<?php $socials = array('facebook'=>'fa-facebook', 'twitter'=>'fa-twitter', 'linkedin'=>'fa-linkedin', 'pinterest'=>'fa-pinterest','tumblr'=>'fa-tumblr', 'googleplus'=>'fa-google-plus');
								  foreach( $socials as $social => $clas ):  
								  
									if( $value = get_user_meta(get_the_author_meta('ID'), $social, true)):?>
							<li class="<?php echo esc_attr($social); ?>"> <a href="<?php echo esc_url( $value ); ?>"><i class="fa <?php echo esc_attr($clas); ?>"></i> </a></li>
							<?php endif; ?>
							<?php endforeach; ?>
						</ul>
					</div>