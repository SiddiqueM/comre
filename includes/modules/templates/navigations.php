					
					<!--=======  POST NAVIGATION =========-->
					<div class="post-navi">
						<ul class="row">
							<?php $prev_post = get_previous_post();
								  $next_post = get_next_post(); ?>
							
							<?php if( $prev_post ): ?>
								<!--=======  PREVIES POST =========-->
								<li class="col-md-6">
									<div class="row">
										<div class="col-xs-4"> <a href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php echo get_the_title( $prev_post->ID ); ?>"> <?php echo get_the_post_thumbnail( $prev_post->ID, 'thumbnail', array('class' => 'img-responsive') ); ?> </a> </div>
										<div class="col-xs-8"> <a href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php echo get_the_title( $prev_post->ID ); ?>"> <span><i class="fa fa-angle-double-left"></i>
											<?php esc_html_e('Previous Post', 'comre');?>
											</span> <span class="hiding"><?php echo get_the_title( $prev_post->ID ); ?></span> </a> </div>
									</div>
								</li>
							<?php endif; ?>

							<?php if( $next_post ): ?>
								<!--=======  NEXT POST =========-->
								<li class="col-md-6">
									<div class="row">
										<div class="col-xs-8 text-right"> <a href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo get_the_title( $next_post->ID ); ?>"> <span>
											<?php esc_html_e('Next Post', 'comre')?>
											<i class="fa fa-angle-double-right"></i></span> <span class="hiding"><?php echo get_the_title( $next_post->ID ); ?></span> </a> </div>
										<div class="col-xs-4"> <a href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo get_the_title( $next_post->ID ); ?>"> <?php echo get_the_post_thumbnail( $next_post->ID, 'thumbnail', array('class' => 'img-responsive') ); ?> </a> </div>
									</div>
								</li>
							<?php endif; ?>
						</ul>
					</div>