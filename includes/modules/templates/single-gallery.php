					
					<!--======= BLOG POST=========-->
					<?php $meta = _WSH()->get_meta(); 
					$gallery = sh_set( $meta, 'sh_gallery_imgs' );?>
						
					<div class="blog-post">
						<?php if( $gallery ): ?>
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
							 <div class="carousel-inner" role="gallery">
						<?php foreach ($gallery as $key => $value) : //echo $gallery[0]['gallery_image'];exit();
						//printr($gallery); exit();?>

								<div class="item <?php  echo ($key == 0) ? 'active' : ''; ?>">
									<img src="<?php echo esc_url(sh_set($value, 'gallery_image')); ?>" >
								</div>

							<?php endforeach; ?>
							</div>
							  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
						    	<i class="fa fa-angle-left fa-2x"></i>
						    </span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
						    	<i class="fa fa-angle-right fa-2x"></i></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div> 
						<?php else: ?>
							<?php the_post_thumbnail('830x390', array('class'=>'img-responsive')) ?>
						<?php endif; ?>

						<span class="post-date-big">
						<?php  echo get_the_date('d');?>
						<br>
						<?php echo get_the_date('M')?></span> <a href="<?php the_permalink();?>" class="title-hed">
						<?php the_title(); ?>
						</a> 
						
						<!--======= TAGS =========-->
						<ul class="small-tag">
							<li>
								<?php /* for categories without anchor*/ $term_list = wp_get_post_terms(get_the_id(), 'category', array("fields" => "names")); ?>
								<span><i class="fa fa-tag"></i><?php echo implode( ', ', (array)$term_list );?></span> / <span><i class="fa fa-user"></i>
								<?php esc_html_e('By', 'comre')?>
								<?php the_author();?>
								</span> / <span> <i class="fa fa-comments"></i>
								<?php comments_number();?>
								</span> / <span> <i class="fa fa-eye"></i>  <a class="post_view" href="javascript:void(0);"><?php echo _WSH()->post_views(); ?> <?php esc_html_e('Viewers', 'comre'); ?></a></span> </li>
						</ul>
						
						<?php the_content();?>
						
						<div class="tags"></div>
						<?php the_tags(); ?>
					</div>
	