<?php if( !defined('ABSPATH') ) die('Restricted Access'); 

   $count = 0;

   $cat = isset( $cat ) ? $cat : array();
   $query_args = array('taxonomy' => 'coupons_store_category' ,'hide_empty' => 0 , 'include' => array($cat), 'posts_per_page' => $num , 'order_by' => $sort , 'order' => $order);
   //if( $cat ) $query_args['coupons_category'] = $cat;
   //echo balanceTags($cat); exit('sssss');
   //$query = new WP_Query( $query_args );
   $terms = get_categories($query_args);
   //print_r($terms); exit();
   ob_start() ;

   ?>
<section class="top-w-deal">
    <div class="container"> 
      <!--======= TITTLE =========-->
      <div class="tittle">
        <h3><?php echo balanceTags($title); ?></h3>
      </div>
      <ul class="row">
        
        <!--======= WEEK DEAL 1 =========-->
        					<?php //while($query->have_posts()): $query->the_post();
                  foreach ($terms as $term):
                    $meta = _WSH()->get_term_meta('_sh_'.$term->taxonomy.'_settings', $term->term_id);
								//global $post ; 
								//$post_meta = _WSH()->get_meta();

                
                if ($deal_column == 3) {

                  $topdeal_columns = '33%';

                }
                elseif ($deal_column == 4){

                   $topdeal_columns = '25%';

                }
                else {
                    $topdeal_columns = '20%';
                }
                //printr ($deal_column);

							    ?>
        
        <li style="width:<?php echo $topdeal_columns?>">
          <div class="w-deal-in" >
		  <img src="<?php echo sh_set($meta,'bg_image') ?>" alt="" class="img-responsive" />
		  <?php //the_post_thumbnail('119x60',array('class'=>'img-responsive'));?> 
            <p><?php echo balanceTags($term->name);?></p>
            
            <!--======= HOVER DETAL =========-->
            <div class="w-over"> <a href="<?php echo esc_url(get_term_link($term, 'coupons_store_category'));?>"><?php esc_html_e('show deal', 'comre');?></a> </div>
          </div>
        </li>
        <?php endforeach //endwhile; 
		//wp_reset_query(); ?>
         </ul>
    </div>
  </section>


<?php return ob_get_clean();