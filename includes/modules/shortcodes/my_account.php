<?php ob_start(); 

$permalink = get_permalink();?>


<?php if( !is_user_logged_in() ): ?>

    <?php $login_page = _WSh()->option('login_page');
    if( $login_page ) {
        $page_content = get_page($login_page);
        if( !is_wp_error($page_content) ) echo do_shortcode($page_content->post_content);
        else echo new WP_Error('post_data_missing', __( 'You need to login or sign up to access this page.', 'comre' ));
    }
    else echo new WP_Error('post_data_missing', __( 'You need to login or sign up to access this page.', 'comre')); ?>

<?php return ob_get_clean();

else: ?>

<section class="comre-my-account-page" id="portfolio">

	<?php $current_user = wp_get_current_user();?>

	<section class="profile-top">
		<div class="container">
			<div class="author mrgb6x mrgt6x row">
				<div class="col-md-2">
				
				<?php if( $thumb_id = get_user_meta( $current_user->ID, 'profile_thumbnail_id', true ) )
				echo wp_get_attachment_image( $thumb_id, 'thumbnail' ); 
				else echo get_avatar( wp_get_current_user()->ID , 70 ); ?>
				
				</div>
				<div class="col-md-9">
					<span class="sec-title"><?php echo get_the_author_meta( 'display_name', $current_user->ID ); ?></span>
					<p><?php echo get_the_author_meta( 'description', $current_user->ID ); ?> </p>
				</div>
				<div class="col-md-1">
					<a href="<?php echo wp_logout_url(home_url()); ?>" title="<?php esc_html_e('Log Out', 'comre'); ?>" class="btn pull-right"><?php esc_html_e('Log Out', 'comre'); ?></a>
				</div>
			</div>
		</div>
	</section>

    <div class="container">

        <div class="row">

            <div class="col-md-2">

                <?php $current_tab = esc_attr( sh_set( $_GET, 'comre_current_tab' ) ); ?>
                <ul class="nav nav-tabs tabs-left">

                    <?php $active = ( !$current_tab || $current_tab == 'profile') ? ' class="active"' : ''; ?>
                    <li <?php echo balanceTags($active); ?>>
                        <a href="<?php echo get_permalink(); ?>"><?php esc_html_e('Profile', 'comre' ); ?></a>
                    </li>
					
					<!---------Coupon Tab---------->

                    <?php $active = ( $current_tab == 'coupon') ? ' class="active"' : ''; ?>

                    <?php if( $coupon ): 

                        $link = add_query_arg(array('comre_current_tab'=>'coupon'), $permalink);?>
                    
                        <li <?php echo balanceTags($active); ?>>
                            <a href="<?php echo esc_url($link); ?>"><?php esc_html_e('Coupons', 'comre'); ?></a>
                        </li>
                    
                    <?php endif; ?>
					
					<!---------Blog Tab---------->
                    
                    <?php $active = ( $current_tab == 'blog') ? ' class="active"' : ''; ?>

                    <?php if( $blog ): 

                        $link = add_query_arg(array('comre_current_tab'=>'blog'), $permalink );?>
                    
                        <li <?php echo balanceTags($active); ?>>
                            <a href="<?php echo esc_url($link); ?>"><?php esc_html_e('Blog', 'comre'); ?></a>
                        </li>
                    
                    <?php endif; ?>
					
					<!---------Store Tab---------->
					
					<?php if( $store ): 
						$active = ( $current_tab == 'store') ? ' class="active"' : '';
                        $link = add_query_arg(array('comre_current_tab'=>'store'), $permalink);?>
                    
                        <li <?php echo balanceTags($active); ?>>
                            <a href="<?php echo esc_url($link); ?>"><?php esc_html_e('Stores', 'comre'); ?></a>
                        </li>
                    
                    <?php endif; ?>
					
					<!---------Order Tab---------->
					
					<?php if( $order ): 
						$active = ( $current_tab == 'woo_orders') ? ' class="active"' : '';
                        $link = add_query_arg(array('comre_current_tab'=>'woo_orders'), $permalink);?>
                    
                        <li <?php echo balanceTags($active); ?>>
                            <a href="<?php echo esc_url($link); ?>"><?php esc_html_e('Orders', 'comre'); ?></a>
                        </li>
                    
                    <?php endif; ?>
					
					<!---------Cred Points Tab---------->
					
					<?php if( $points ): 
						$active = ( $current_tab == 'cred') ? ' class="active"' : '';
                        $link = add_query_arg(array('comre_current_tab'=>'cred'), $permalink);?>
                    
                        <li <?php echo balanceTags($active); ?>>
                            <a href="<?php echo esc_url($link); ?>"><?php esc_html_e('Points', 'comre'); ?></a>
                        </li>
                    
                    <?php endif; ?>
					

                </ul>
            
            </div>

            <div class="col-md-10">

                <div class="tab-content">

                    <?php $active = ( !$current_tab || $current_tab == 'profile') ? true : false; ?>

                    <?php if( $active ): ?>
                        
                        <div class="tab-pane fade in active">
                            <h5><?php esc_html_e('Profile', 'comre' ); ?></h5>
                            <?php do_action('wt_account_manager_users_form'); ?>
                        </div>
                    
                    <?php endif;

                    $active = ( $current_tab == 'coupon' ) ? true : false;
					
					$permalink = get_permalink();
		
					$link_params = $_GET;//print_r($link_params);
					
					if( isset( $link_params['wtam_post_id'] ) )  unset( $link_params['wtam_post_id'] );
					$link_params['wtam_current_page'] = 'coupon_form'; 
					
					$add_new_link = add_query_arg( $link_params, $permalink ); 
					
					//Coupons tab content

                    if( $active && $coupon ): ?>
                        <div class="tab-pane fade in active">
                            
                            <h5><?php esc_html_e('Coupons', 'comre'); ?></h5>
							<a href="<?php echo $add_new_link; ?>" class="pull-right btn add-new"><?php esc_html_e('Add New Coupon', 'comre' ); ?></a>
                            
                            <?php if(isset($_GET['wtam_current_page']) && esc_attr($_GET['wtam_current_page']) == 'coupon_form' )
                            {
                                do_action('wt_account_manager_coupons_form');   
                            }
                            else {
                                wt_account_manager_get_template_part('coupon/coupon_listing');
                            } ?>

                        </div>
                    <?php endif;
					
					//Blog tab content

                    $active = ( $current_tab == 'blog' ) ? true : false;
					
					$permalink = get_permalink();
		
					$link_params = $_GET;//print_r($link_params);
					
					if( isset( $link_params['wtam_post_id'] ) )  unset( $link_params['wtam_post_id'] );
					$link_params['wtam_current_page'] = 'post_form'; 
					
					$add_new_link = add_query_arg( $link_params, $permalink ); 

                    if( $active && $blog ): ?>
                        
                        <div class="tab-pane fade in active">
                            
                            <h5><?php esc_html_e('Blog', 'comre') ?></h5>
							<a href="<?php echo $add_new_link; ?>" class="pull-right btn add-new"><?php esc_html_e('Add New Post', 'comre' ); ?></a>
                            
                            <?php if(isset($_GET['wtam_current_page']) && esc_attr($_GET['wtam_current_page']) == 'post_form' )
                            {
                                do_action('wt_account_manager_post_form');  
                            }
                            else {
                                wt_account_manager_get_template_part('post/post_listing');
                            } ?>
                        
                        </div>
                    
                    <?php endif; 
					
					//Store tab content
					
					$active = ( $current_tab == 'store' ) ? true : false;
					
					$permalink = get_permalink();
		
					$link_params = $_GET;//print_r($link_params);
					
					if( isset( $link_params['wtam_term_id'] ) )  unset( $link_params['wtam_term_id'] );
					$link_params['wtam_current_page'] = 'store_form'; 
					
					$add_new_link = add_query_arg( $link_params, $permalink ); 
					
					

                    if( $active && $store ): ?>
                        
                        <div class="tab-pane fade in active">
                            
                            <h5><?php esc_html_e('Store', 'comre') ?></h5>
							<a href="<?php echo $add_new_link; ?>" class="pull-right btn add-new"><?php esc_html_e('Add New Store', 'comre' ); ?></a>
                            
                            <?php if(isset($_GET['wtam_current_page']) && esc_attr($_GET['wtam_current_page']) == 'store_form' )
                            {
                                do_action('wt_account_manager_store_form');  
                            }
                            else {
                                wt_account_manager_get_template_part('store/store_listing');
                            } ?>
                        
                        </div>
                    
                    <?php endif; 
					
					//Orders tab content
					
					$active = ( $current_tab == 'woo_orders' ) ? true : false;
					
					$permalink = get_permalink();
		
					$link_params = $_GET;//print_r($link_params);
					
					if( isset( $link_params['wtam_term_id'] ) )  unset( $link_params['wtam_term_id'] );
					$link_params['wtam_current_page'] = 'woo_order_form'; 
					
					$add_new_link = add_query_arg( $link_params, $permalink ); 

                    if( $active && $order && function_exists('WC') ): ?>
                        
                        <div class="tab-pane fade in active">
							
                            
                            <?php wt_account_manager_get_template_part('woo_orders/woo_orders_listing'); ?>
                        
                        </div>
                    
                    <?php endif; 
					
					
					//Cred Points tab content
					
					$active = ( $current_tab == 'cred' ) ? true : false;
					
					//$permalink = get_permalink();
		
					//$link_params = $_GET;//print_r($link_params);
					
					//if( isset( $link_params['wtam_term_id'] ) )  unset( $link_params['wtam_term_id'] );
					//$link_params['wtam_current_page'] = 'cred_form'; 
					
					//$add_new_link = add_query_arg( $link_params, $permalink ); 
					
					if( $active && $points ): ?>
                        
                        <div class="tab-pane fade in active">
							
                            
                            <?php wt_account_manager_get_template_part('cred/cred_listing'); ?>
                        
                        </div>
					<?php endif; ?>
					

                </div>
        
            </div>

        </div>
    </div>
</section>


<?php return ob_get_clean();

endif;