<?php if(!defined('ABSPATH')) die('Restricted Access'); ?>


<?php 

/**
 * The template for displaying the woocommerce products in grid view and masonry style.
 *
 * Override this template by copying it to comre-child/includes/modules/shortcodes/products.php
 *
 * @author    WoWThemes
 * @package   Modules/Shortcodes
 * @version   1.0.1
 */

wp_enqueue_script(array('jquery-isotope')); 

$paged = get_query_var('paged');
$query_args = array('post_type' => 'product' , 'showposts' => $num , 'orderby' => $sort , 'order' => $order, 'paged'=>$paged);

$cat = sh_set( $_GET, 'sort_by_category' );   

if( $cat ) $query_args['product_cat'] = $cat;
//echo balanceTags($cat); exit('sssss');
$query = new WP_Query($query_args);

$ext = explode( ',', $extras );  
$filteration = array();
$posts_data = '';

ob_start(); ?>


<?php while( $query->have_posts() ): $query->the_post(); ?>

    <?php global $product;
    
    $shipping_text = esc_html__('FREE SHIPPING', 'comre'); 
    $meta = wp_parse_args( _WSH()->get_meta(), array('cashback'=>'', 'upto'=>'') ); //_WSH()->get_meta();
    extract( $meta );

    $shipping_class = $product->get_shipping_class();
    if( $shipping_class )
    {
        $shipping_term = get_term_by( 'slug', $shipping_class, 'product_shipping_class');
        if( !is_wp_error( $shipping_term ) ) $shipping_text = $shipping_term->name;
    }

    $terms = wp_get_post_terms( get_the_id(), 'product_cat' );
    $current_terms = array();
    foreach( $terms as $t ) {
        $filteration[$t->term_id] = '<li><a href="#" data-filter=".'.$t->slug.'">'.$t->name.'</a></li>'; 
        $current_terms[$t->term_id] = $t->slug;
    }
    
    if( $cat ) $filteration = array();?>

    <!--======= ITEM =========-->
    <li class="item store <?php echo implode(' ', $current_terms ); ?>">
        <div class="prod-item">
            <div class="top-brand">
                <?php the_post_thumbnail('114x42'); ?> 
                
                <div class="up-to">
                    <?php if( $upto ): ?>
                         <span><?php echo wp_kses_post( $upto ); ?></span> 
                    <?php endif; ?>
                    
                    <?php if( $cashback ): ?>
                        <span><?php echo wp_kses_post( $cashback ); ?></span>
                    <?php endif; ?>
                </div>

            </div>

            <!--======= ITEM IMAGE =========--> 
            <?php the_post_thumbnail('242x229',array('class'=>'img-responsive img-product')) ?>
            <h5><a href="<?php the_permalink();?>"> <?php the_title();?> </a></h5>

            <!--======= ITEM INFO =========-->
            <div class="items-info">
                <h5><?php woocommerce_template_single_price(); ?></h5>
                <span class="free-ship"><?php echo esc_attr( $shipping_text ); ?></span>
                <div class="clearfix"></div>
                <?php if(sh_set($ext, 'shop_btn')):?>
                    <a href="<?php the_permalink();?>" class="btn"><?php esc_html_e('SHOP NOW', 'comre');?></a>
                <?php endif;?>
            </div>
        </div>
    </li>

<?php endwhile; 
wp_reset_postdata();

$posts_data = ob_get_clean();	

ob_start();?>


<!--======= PORTFOLIO =========-->
<section id="portfolio">
    <div class="portfolio portfolio-filter"> 

        <!--======= PORTFOLIO ITEMS =========-->
        <div class="portfolio-wrapper">
            <div class="container"> 



                <!--======= PORTFOLIO FILTER =========-->
                <ul class="filter">
                    <li><a class="active" href="#." data-filter="*"><?php esc_html_e('All  offers', 'comre'); ?></a></li>

                    <?php if( $filteration )
                    foreach( $filteration as $filter )
                        echo balanceTags($filter);?>

                    <li class="pull-right">
                        <?php $all_terms = get_terms( 'product_cat' ); 

                        if( $all_terms ):?>
                            
                            <form action="<?php echo get_permalink();?>" id="product_sort_by_category">
                                <select name="sort_by_category">
                                    <option value=""><?php esc_html_e('All', 'comre');?></option>
                                    <?php foreach( $all_terms as $a_term ): ?>
                                        <option value="<?php echo $a_term->slug; ?>" <?php selected($a_term->slug, $cat); ?>><?php echo $a_term->name; ?></option>
                                    <?php endforeach; ?> 

                                </select>
                            </form>
                        <?php endif; ?>
                    </li>
                </ul>

                <!--======= ITEMS =========-->
                <ul class="items">
                    <?php echo balanceTags( $posts_data ); ?>
                </ul>

                <?php _the_pagination(array('total'=>$query->max_num_pages)); ?>

            </div>
        </div>
    </div>

</section>

<?php return ob_get_clean();