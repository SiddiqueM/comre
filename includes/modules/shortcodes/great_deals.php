<?php
/**
 * The template for displaying the coupons in grid view.
 *
 * Override this template by copying it to comre-child/includes/modules/shortcodes/great_deals.php
 *
 * @author    WoWThemes
 * @package   Comre
 * @version     1.2.0
 */

wp_enqueue_script( array( 'zeroclipboard', 'wc-add-to-cart' ) );


$ext = explode( ',', $extras );
ob_start();

$settings = _WSH()->option();

$coupon_column = ( $coupon_column ) ? $coupon_column : 3;

// Hookup custom columns with coupons.
$coupon_column = apply_filters( 'comre_grea_deals_coupon_columns', $coupon_column );

$permalink = get_permalink();
$paged = get_query_var( 'paged' );

if ( ! $paged ) {
	$paged = sh_set( $_GET, 'paged' ) ? sh_set( $_GET, 'paged' ) : 1;
}

$load_num = balanceTags( $num_load_more );
//$load_num = sh_set( $_GET, 'num_load_more' ) ? sh_set( $_GET, 'num_load_more' ) : $load_num;
$post_meta_one = '';

$deal_view_class = ( 'list' === $deal_view ) ? ' list_style' : '';

$num = ( $paged > 1 ) ? $load_num : $num;
$count = 0;
$query_args = array( 'post_type' => 'sh_coupons' , 'showposts' => $num , 'orderby' => $sort , 'order' => $order, 'paged' => $paged );

if ( $cat ) {
	$query_args['coupons_category'] = $cat;
}

$query = new WP_Query( $query_args );

?>


<!--======= BANNER =========-->
<section class="great-deals">

	<div class="container"> 

		<!--======= TITTLE =========-->
		<div class="tittle"> 
			<h3><?php echo wp_kses_post( $title ); ?></h3>
		</div>

		<div class="coupon woocommerce">
			<?php if ( $query->have_posts() ) : ?>

				<ul class="row _great_deals<?php echo esc_attr( $deal_view_class ); ?>">
				
					<!--======= COUPEN DEALS =========-->
					<?php while ( $query->have_posts() ) : $query->the_post();

						global $product;

								
						$post_meta = _WSH()->get_meta();
						$is_purchaseable = sh_set( $post_meta, 'is_purchaseable' );
						
						if ( $is_purchaseable ) {
						
							//$product = new WC_Product( get_the_id() );
							$product = wc_get_product( get_the_id() );
							//$product->product_type = ($product->get_type()) ? $product->get_type() : 'simple';
						}
						//printr($product);
						$post_meta = _WSH()->get_meta();
						$post_meta_one = ($post_meta) ? $post_meta : $post_meta_one;

						$last_class = ( 0 === ( $query->current_post % ( $coupon_column ) ) ) ? ' first' : '';
						$copoun_columns = ( 3 === $coupon_column ) ? 'col-md-4 col-sm-6' : 'col-md-3 col-sm-6';

						$copoun_columns = apply_filters( 'comre_great_deals_column_class', $copoun_columns );

				       	$buton_title = ( sh_set( $post_meta, 'buttons_title' ) ) ? sh_set( $post_meta, 'buttons_title' ) : 'get coupon code'; ?>

						<li class="<?php  echo esc_attr( $copoun_columns . $last_class );?>">
					
							<div class="coupon-inner">
								
								
								<?php do_action( 'comre_great_deals_top_tag', $ext, $post_meta ); ?>

								
								<div class="c-img">
								
									<?php do_action( 'comre_great_deals_post_content', '324x143' ); ?>

									<div class="coupon-info-box">

										<?php do_action( 'comre_great_deals_info_box', $post_meta, $ext, $count, $is_purchaseable ); ?>

									</div>

								</div>

								<?php do_action( 'comre_great_deals_btm_info', $post_meta, $ext ); ?>
								
		                        
		                    </div>
					
						
						
						</li>

						<?php $count++;

	                endwhile;

	                wp_reset_postdata();?>
				
				<!--======= COUPEN DEALS =========-->
				
				</ul>
			
			<div class="load_more_btn">
            <center><a title="<?php esc_html_e('Load More', 'comre'); ?>" href="javascript:void(0);" class="btn-primary btn ajax_load_more">
				<?php esc_html_e('Load More', 'comre'); ?>
			</a></center>
            </div>
		</div>
        <?php endif?>
		
	</div>

	<?php //Using custom function to include templates. It can be override from Child Theme.
	// by placing coupon_tabs.php in child-theme/includes/modules/coupons/
	//print_r($post_meta_one);exit;
	_WSH()->template_part('includes/modules/coupons/coupon_code_modal', '', array('post_meta'=>$post_meta_one)); ?>

	
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.get_coupon_code').each(function(index, element) {
				var elem_id = $(this).attr('id');
				var coupon_code = $(this).data('text');
				
				if( coupon_code ) {
					$("#"+elem_id).zclip({
						path: '<?php echo get_template_directory_uri(); ?>/js/ZeroClipboard.swf',
						copy: coupon_code,
						afterCopy: function() {
						   console.log('copied');
						  alert('Data in clipboard! Now you can paste it somewhere');
						}
					});
				}
			});
			
		});
	</script>
</section>
<script type="text/javascript">
	
	jQuery(document).ready(function($){

		try{
			
			$('.ajax_load_more').on('click', function(e){
				e.preventDefault();
				var thiselem = this;
				var paged = $(this).data('paged');
				paged = ( paged === undefined ) ? 2 : paged;
				var load_more = $(this).data('load_more');
				load_more = (load_more===undefined) ? true : load_more;
				
				if( load_more === false ) {
					$(this).html('No More data to load');	
					return;
				}

				var permalink = '<?php echo esc_url($permalink); ?>';

				var old_text = $(this).text();
				$(this).html(old_text + '<i class="fa fa-refresh fa-spin"></i>');

				$.ajax({
					url: permalink+'?paged='+paged,
					type: 'GET',
					data: { paged : paged, num_load_more: <?php echo esc_attr( $num_load_more ); ?>},
					success: function(res) {
						$(thiselem).html(old_text);
						var newpage = paged + 1;
						var content = $('._great_deals > li', res);
						console.log(content.length);
						if( content.length ) {
							$(thiselem).data('paged', newpage );
							$('._great_deals').append(content);//.masonry('appended', content);
						}
						else {
							$(thiselem).data('load_more', false);
							$(thiselem).html('No More data to load');
						}
					},
					error: function(res) {
						$(thiselem).data('load_more', false);
						
						$(thiselem).html(old_text);
					}
				});
			});

		}
		catch(e) {
			console.log(e.message);
		}
	});
	
</script>

<?php return ob_get_clean();