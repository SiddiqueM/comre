<?php if( !defined('ABSPATH') ) die('Restricted Access');

/**
 * The template for displaying the coupons in owl carousel.
 *
 * Override this template by copying it to comre-child/includes/modules/shortcodes/top_offers.php
 *
 * @author    WoWThemes
 * @package   Modules/Shortcodes
 * @version     1.1.0
 */

wp_enqueue_script(array('owl-carousel'));

global $post ;
$count = 0;
$query_args = array('taxonomy' => 'coupons_store_category' ,'hide_empty' => 0 , 'include' => array($cat), 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
$terms = get_categories($query_args);

ob_start() ;?>

<section class="top-offer">

    <div class="container"> 

        <!--======= TITTLE =========-->
        <div class="tittle">
            <h3><?php echo balanceTags($title);?></h3>
        </div>

        <div class="today-top"> 

            <!--======= COUPON 1 =========-->
            <?php
            foreach ($terms as $term):
                
                $meta = _WSH()->get_term_meta('_sh_'.$term->taxonomy.'_settings', $term->term_id);


                //$post_meta = _WSH()->get_meta();

                $topoffers_column = ($topoffer_column == 4 ) ? 280 : 380; ?>

                <div class="offer-in"><img src="<?php echo sh_set($meta,'bg_image') ?>" class="img-responsive store-img">  

                    <div class="offer-top-in">

                        <h6><?php echo balanceTags($term->name); ?></h6>

                        <div class="btm-offer">
                            <p class="text-uppercase"><?php echo sh_set($meta, 'cashback');?></p>
                        </div>

                    </div>

                    <!--======= COUPON HOVER =========-->
                    <div class="off-over">
                        <h6> <?php echo balanceTags($term->name); ?> </h6>

                        <a href="<?php echo esc_url(get_term_link($term, 'coupons_store_category'));?>" class="btn"><?php esc_html_e('GO TO STORE','comre') ;?></a>

                        <div class="btm-offer">
                            <p class="text-uppercase"><?php echo sh_set($meta, 'cashback');?></p>
                        </div>
                    </div>
                </div>

            <?php endforeach;
            //wp_reset_postdata();?>
        
        </div>
    
    </div>

</section>


<script>
  jQuery(document).ready(function($) {


    if( $('.today-top').length ) {
      $('.today-top').owlCarousel({
        loop:true,
      autoPlay: 6000, //Set AutoPlay to 6 seconds 
      items : <?php echo ($topoffer_column) ? esc_attr($topoffer_column) : 4; ?>,
      margin:30,
      navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
      responsiveClass:true,
      responsive:{
        0:{
          items:1,
          nav:true
      },
      600:{
          items:2,
          nav:false
      },
      1000:{
          items:<?php echo ($topoffer_column) ? esc_attr($topoffer_column) : 4; ?>,
          nav:true,
          loop:false
      }}
  });
  }

});


</script>

<?php return ob_get_clean();