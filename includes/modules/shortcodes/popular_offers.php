<?php if( !defined('ABSPATH') ) die('Restricted Access'); 

/**
 * The template for displaying the coupons in grid view.
 *
 * Override this template by copying it to comre-child/includes/modules/shortcodes/popular_offers.php
 *
 * @author    WoWThemes
 * @package   Modules/Shortcodes
 * @version     1.1.0
 */


global $post;

   $count = 0; 

   $query_args = array('post_type' => 'sh_coupons' , 'showposts' => $num , 'orderby' => $sort , 'order' => $order);
   
   if( $cat ) {
	   $termby_slug = get_term_by('slug', $cat, 'coupons_category');
	   
	   if( $termby_slug ) {
		   $child = get_terms( 'coupons_category', array('parent'=>$termby_slug->term_id, 'fields'=>'ids')); //printr($child);
		   if($child) $query_args['tax_query'] = array(array('taxonomy' => 'coupons_category','field'    => 'slug','terms'    => $child));
		   else $query_args['coupons_category'] = $cat;
	   }
	   else $query_args['coupons_category'] = $cat;
   }

   
	$query = new WP_Query($query_args);
    
	$filteration = array();
	$posts_data = array();

 ?>
<?php $active_term = '';

while( $query->have_posts() ): $query->the_post(); 
	
	$meta = _WSH()->get_meta();
	$offers_columns = ($offer_column == 3) ? 'col-md-4 col-sm-4' : 'col-md-3 col-sm-3';?>

	<?php $terms = wp_get_post_terms( get_the_id(), 'coupons_category' );
	$active = ( $query->current_post == 0 ) ? ' active' : '';
    
    foreach( $terms as $t ) :
   
        if($active) $active_term = $t->term_id;
    	
    	$term_active = ( $active_term == $t->term_id ) ? ' active' : '';
		$filteration[$t->term_id] = '<li role="presentation" class="col-xs-2'.$term_active.'"><a href="#'.$t->slug.'"  role="tab" data-toggle="tab">'.$t->name.'</a></li>'; ?>
		
		<?php ob_start(); ?>

		<!--======= ROW =========-->
		
		<?php //Using custom function to include templates. It can be override from Child Theme.
		// by placing coupon_tabs.php in child-theme/includes/modules/coupons/
		
		_WSH()->template_part('includes/modules/coupons/coupon_tabs', '', array('meta'=>$meta, 'offers_columns'=>$offers_columns, 'count' => $count )); ?>

		<?php $posts_data[$t->slug][get_the_id()] = ob_get_clean();
   
    endforeach; ?>

	<?php $count++; 

endwhile; 

wp_reset_postdata(); ?>

<?php ob_start();?>

<?php //Using custom function to include templates. It can be override from Child Theme.
// by placing coupon_tabs.php in child-theme/includes/modules/coupons/
_WSH()->template_part('includes/modules/coupons/coupon_tabs_main', '', array('meta'=>$meta, 'posts_data'=>$posts_data, 'filteration' => $filteration )); ?>


<?php return ob_get_clean();