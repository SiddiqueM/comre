<?php ob_start();

$options = _WSH()->option();
$map_api = (sh_set( $options, 'map_api')) ? sh_set( $options, 'map_api') : '';

$lat = ( $lat ) ? $lat : '-37.8176419';
$long = ( $long ) ? $long : '144.9554397';
$address = ($address) ? $address : '121 King Street Melbourne Victoria 3000 Australia';
$marker = ( $marker ) ? wp_get_attachment_url($marker) : get_template_directory_uri().'/images/map-locator.png'; //print_r($marker);exit;
 ?>

<section class="contact"> 
    
    <!--======= MAP =========-->
    <div id="map"></div>
</section>

<!-- MAP --> 
<?php 
$protocol= ( is_ssl() ) ? 'https' : 'http';

?>
<script type='text/javascript' src='<?php echo esc_attr( $protocol );  ?>://maps.google.com/maps/api/js?key=<?php echo $map_api; ?>'></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mapmarker.js"></script> 
<script type="text/javascript">
	// Use below link if you want to get latitude & longitude
	// http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude.php	
	jQuery(document).ready(function($){
	
		//set up markers 
		var myMarkers = {"markers": [
				{"latitude": "<?php echo esc_js($lat);?>", "longitude":"<?php echo esc_js($long);?>", "icon": "<?php echo esc_js($marker);?>", "baloon_text": '<?php echo esc_js($address);?>'}
			]
		};
		
		//set up map options
		$("#map").mapmarker({
			zoom	: 15,
			center	: '<?php echo esc_js($address);?>',
			markers	: myMarkers
		});

	});
</script>

<?php return ob_get_clean();