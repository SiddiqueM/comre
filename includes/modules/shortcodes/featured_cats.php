<?php wp_enqueue_script(array('jquery', 'owl-carousel'));
   $count = 0;
   $term_args = array('hide_empty' => $empty , 'number' => $num , 'orderby' => $sort , 'order' => $order, 'include' => array($comre_tax));
   //if( $cat ) $query_args['category_name'] = $cat;
   //echo balanceTags($cat); exit('sssss');
   $terms = get_terms( 'product_cat', $term_args) ; 
   //printr($terms);
   ob_start() ;?>

  <?php $feature_column = ($feature_column) ? $feature_column : 4;
  $features_column = ($feature_column == 4 ) ? 280 : 380; 
  //print_r($features_column); exit();
  if ($comre_tax == 'wocommerce_category') :     

  ?>
   
  <section class="featured <?php echo $features_column ?>">
    <div class="container"> 
      <!--======= TITTLE =========-->
      <div class="tittle">
        <h3><?php echo balanceTags($title);?></h3>
      </div>
      <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):?>
      <div class="fea-cate"> 
        
        <?php foreach ( $terms as $term ) :
			$meta = _WSH()->get_term_meta( '_sh_product_cat_settings', $term->term_id );//printr($term); exit();
		?>
        
        <!--======= CATEGORIES 1 =========-->
        <div class="cate-in">
		
		  <?php   $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>
          <div class="cate"> 
		  
		  	<?php echo wp_get_attachment_image( $thumbnail_id, 'full' ); ?>
            <div class="cate-over"> <i class="fa <?php echo esc_attr(sh_set($meta, 'product_icon'));?>"></i>
              <div class="after-over animated flipInY">
                <p><?php echo sprintf( _n( '1 coupon inside', '%s coupons inside', $term->count, 'comre'), $term->count );?> </p>
                <a href="<?php echo esc_url(get_term_link($term, 'product_cat'));?>"><?php esc_html_e('SHOP NOW', 'comre');?></a> </div>
            </div>
            <!--======= TITTLE =========-->
            <div class="cate-tittle"> <?php echo balanceTags($term->name);?> </div>
          </div>
        </div>
        
        <?php endforeach;?>
      
      </div>
      
	  <?php endif;?>
    
    </div>
  </section>

<?php elseif ($comre_tax == 'coupons_category') :
  $query_args = array('taxonomy' => 'coupons_category' ,'hide_empty' => 0 , 'include' => array($comre_tax), 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
  $terms = get_categories($query_args);
 ?>
<section class="featured <?php echo $features_column ?>">
    <div class="container"> 
      <!--======= TITTLE =========-->
      <div class="tittle">
        <h3><?php echo balanceTags($title);?></h3>
      </div>
      <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):?>
      <div class="fea-cate"> 
        
        <?php foreach ( $terms as $term ) :
      $meta = _WSH()->get_term_meta('_sh_'.$term->taxonomy.'_settings', $term->term_id); //printr($meta); exit();
    ?>
        
        <!--======= CATEGORIES 1 =========-->
        <div class="cate-in">
    
            
          <div class="cate">
            <img src="<?php echo sh_set($meta,'bg_image') ?>" class="img-responsive store-img"> 
            <div class="cate-over"> <i class="fa <?php echo esc_attr(sh_set($meta, 'product_icon'));?>"></i>
              <div class="after-over animated flipInY">
                <p><?php echo sprintf( _n( '1 coupon inside', '%s coupons inside', $term->count, 'comre'), $term->count );?> </p>
                <a href="<?php echo esc_url(get_term_link($term, 'coupons_store_category'));?>"><?php esc_html_e('SHOP NOW', 'comre');?></a>
              </div>
            </div>
            <!--======= TITTLE =========-->
            <div class="cate-tittle"> <?php echo balanceTags($term->name);?> </div>
          </div>
        </div>
        
        <?php endforeach;?>
      
      </div>
      
    <?php endif;?>
    
    </div>
  </section>
  <?php else: 

    $cat = isset($cat) ? $cat : array();

    $query_args = array('taxonomy' => 'coupons_store_category' ,'hide_empty' => 0 , 'include' => array($cat), 'number' => $num , 'order_by' => $sort , 'order' => $order);
    $terms = get_categories($query_args);?>

  <section class="featured <?php echo $features_column ?>">
    <div class="container"> 
      <!--======= TITTLE =========-->
      <div class="tittle">
        <h3><?php echo balanceTags($title);?></h3>
      </div>
      <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):?>
      <div class="fea-cate"> 
        
        <?php foreach ( $terms as $term ) :
      $meta = _WSH()->get_term_meta('_sh_'.$term->taxonomy.'_settings', $term->term_id); //printr($meta);
    ?>
        
        <!--======= CATEGORIES 1 =========-->
        <div class="cate-in">
    
      <?php   $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>
          <div class="cate"> 
      
          <img src="<?php echo sh_set($meta,'bg_image') ?>" class="img-responsive store-img">
            <div class="cate-over"> <i class="fa <?php echo esc_attr(sh_set($meta, 'product_icon'));?>"></i>
              <div class="after-over animated flipInY">
                <p><?php echo sprintf( _n( '1 coupon inside', '%s coupons inside', $term->count, 'comre'), $term->count );?> </p>
                <a href="<?php echo esc_url(get_term_link($term, 'product_cat'));?>"><?php esc_html_e('SHOP NOW', 'comre');?></a> </div>
            </div>
            <!--======= TITTLE =========-->
            <div class="cate-tittle"> <?php echo balanceTags($term->name);?> </div>
          </div>
        </div>
        
        <?php endforeach;?>
      
      </div>
      
    <?php endif;?>
    
    </div>
  </section>
<?php endif; ?>
<script>
jQuery(document).ready(function($) {
   /*$('.fea-cate').owlCarousel({
     slideWidth: <?php echo esc_attr($feature_column); ?>,
     //minSlides: 1,
     maxSlides: 6,
     slideMargin: 30,
   });*/

if( $(".fea-cate").length ) {
            $(".fea-cate").owlCarousel({ 
                autoPlay: 6000, //Set AutoPlay to 6 seconds 
                items :<?php echo esc_attr($feature_column); ?>,
                margin:30,
                responsiveClass:true,
                navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
                //responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    1000:{
                        items:<?php echo esc_attr($feature_column); ?>,
                        nav:true,
                        loop:false
                    }
            }});
    }

});


 </script>

<?php return ob_get_clean();?>

