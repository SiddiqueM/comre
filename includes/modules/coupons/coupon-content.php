<?php
/**
 * Coupon content template
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
} ?>

<div class="coupon-image">
	<?php echo wp_kses_post( comre_wow_themes_coupon_featured_image( $size ) ); ?>
</div>

<div class="coupon-content-box">
	<a class="head" href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
	
	<div class="clearfix"><?php the_excerpt(); ?></div>
</div>
