<?php if( !defined('ABSPATH') ) die('Restricted Access'); ?>

<?php $settings = _WSH()->option();

//$post_meta = _WSH()->get_meta(); 
//print_r($post_meta);
$news_title = sh_set($settings, 'news_title');
$news_form_url = sh_set($settings, 'news_form_url');

$coupon_popup_title = (sh_set($settings, 'coupon_popup')== 'custom') ? $settings['custom_title'] : esc_html__('Get Coupon Code', 'comre') ; ?>
						
<!-- Modal -->
<div class="modal fade" id="myModalCouponCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $coupon_popup_title;?></h4>

			</div>
			<div class="modal-body">
				<i class="fa fa-spinner fa-spin fa-lg col-md-push-6 modal-processing"></i>
			</div>
			<div class="modal-footer">
			
				<?php if(sh_set($settings, 'btn_newsletter')):
					the_widget( 'SH_NewsLetter', array('title' => $news_title, 'mc' => $news_form_url, 'txt' => '') );
				endif; ?>
				
				<!--<button type="button" class="btn btn-default" data-dismiss="modal"><?php //esc_html_e('Close', 'comre');?></button> -->
			</div>
		</div>
	</div>
</div>


<!-- Disqus Modal -->
<div class="modal fade" id="myModaltest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php esc_html_e('Start Discussion with Disqus', 'comre');?></h4>
			</div>
			<div class="modal-body">
				<?php echo sh_set($post_meta, 'coupon_code');?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close', 'comre');?></button>
			</div>
		</div>
	</div>
</div>


