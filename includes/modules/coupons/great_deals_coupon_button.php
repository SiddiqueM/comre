<?php
/**
 * Get coupon code button
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

$options = _WSH()->option();

$ref_type = sh_set( $options, 'coupon_ref_style', 'same_tab' );

$copied = sh_set( $options, 'copy_coupon_code_clipboard', false );

if ( in_array( 'coupon_code', $ext, true ) ) :

	$coupon_link = _set_refferer_to_link( sh_set( $post_meta, 'coupon_link' ) );

	$ext_link = ( $coupon_link ) ? ' data-href="'. esc_url( $coupon_link ).'" target="_blank"' : '';
	$ext_link .= ' data-detail="'.esc_url( get_permalink() ).'" ';

	$cash_back = ( sh_set( $post_meta, 'cashback' ) ) ? sh_set( $post_meta, 'cashback' ) : esc_html__( 'No Cashback', 'comre' );

	$buton_title = ( sh_set( $post_meta, 'buttons_title' ) ) ? sh_set( $post_meta, 'buttons_title' ) : 'get coupon code'; ?>
		
	<h4 class="text-center cash-back"><?php echo esc_attr( $cash_back ); ?></h4>

	<?php if ( $is_purchaseable ) : ?>

		<?php woocommerce_template_loop_add_to_cart(); ?>

	<?php else : ?>
		<?php if ( $copied ) :?>
			
			<div class="text-center" data-id="get_the_coupon_code<?php echo esc_attr( $count ); ?>"> 
				<a<?php echo wp_kses_post( $ext_link ); ?> data-text="<?php echo esc_attr( sh_set( $post_meta, 'coupon_code' ) );?>" class="btn get_coupon_code" id="get_coupon_code<?php echo esc_attr( $count ); ?>" data-referrer="<?php echo esc_attr( $ref_type ); ?>"><?php echo esc_attr( $buton_title );?></a> 
			</div>

		<?php else : ?>
			<div class="text-center"> 
				<a<?php echo wp_kses_post( $ext_link ); ?> class="btn get_coupon_code" data-toggle="modal" data-target="#myModalCouponCode" id="get_coupon_code<?php echo esc_attr( $count ); ?>" onClick="ShowCouponCodeInModal(<?php echo esc_attr( get_the_id() ); ?>, '#myModalCouponCode', this)" data-referrer="<?php echo esc_attr( $ref_type ); ?>"><?php echo esc_attr( $buton_title );?></a> 
			</div>
		<?php endif;?>

	<?php endif; ?>

<?php endif;?>
