<?php
/**
 * Coupon ajax popup
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

$id = isset( $_POST['coupon_id'] ) ? $_POST['coupon_id'] : null;

if( !$id ) exit(esc_html__('<p>Invalid parameters passed to the request</p>', 'comre') );

$post = get_post($id);

if( is_wp_error( $post ) ) exit(esc_html__('<p>Invalide coupon id provided</p>', 'comre') );

$options = _WSH()->option();
$meta = _WSH()->get_meta('_sh_sh_coupons_settings', $id);
$small_image = sh_set( $meta, 'small_image');
$cashback = sh_set( $meta, 'cashback');
$valid = sh_set( $meta, 'expires_date');
$ext_link = sh_set($meta, 'coupon_link'); //print_r($ext_link); exit;
$ext_link =  ($ext_link) ? ' href="'.$ext_link.'" target="_blank"' : ' href="'.get_permalink().'"';
$btn_goto_shop = sh_set($options, 'btn_goto_shop');
$btn_shop = ($btn_goto_shop) ? '<a class="btn modal_goto_shop" '.$ext_link .'>'.esc_html__('Go to Shop', 'comre').'</a>' : '';

if( $coupon_code = sh_set( $meta, 'coupon_code') )
{
	echo '<div class="modal-coupon-code">
					<div class="img-store">
					 	<img src="'.$small_image.'">
					</div>
					<div class="store-content">
					<div class="text">
							'.get_the_term_list($post->ID, "coupons_store_category", __("Stores: ",  "comre"), "," ).'
					</div>
					
					<div class="cashback"><strong>Cashback: </strong>'.$cashback.'</div>
					<div class="cashback"><strong>Valid: </strong>'.$valid.'</div>
				
					
					
					<div class="cashback-text"><p>'.esc_html__("Cashback will be added in your wallet in next 5 Minute of your purchase.", "comre").'</p></div>
					
				</div>
				<div class="clearfix"></div>
				<div class="clearfix modal-footer-one">
					<div class="code-coupon">
						<h5>
							'.esc_html__("Coupon Code: ", "comre").' <span class="coupon-code-wrapper">
							<i class="fa fa-scissors"></i>
							'. $coupon_code . '
							</span>
							'.$btn_shop.' 
						</h5>

					</div>
					
				</div>
		  </div>';
	exit;
}

else {
	exit(esc_html__('No coupon code found for the provided id.', 'comre') );
}

