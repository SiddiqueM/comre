<?php if( !defined('ABSPATH') ) die('Restricted Access'); ?>




<section class="coupen-tab">
	<div role="tabpanel"> 
		
		<!--======= COUPON NAV TAB =========-->
		<div class="tab-role">
			<div class="copo-tab">
				<div class="container">
					<ul class="nav nav-tabs">
						<?php if( $filteration ) echo implode("\n", $filteration ); ?>
					</ul>
				</div>
			</div>
			<div class="container"> 
				<!--======= TAB PANES =========-->
				<div class="tab-content">
					<?php $count = 0; 
					if( $posts_data )
					
					foreach( $posts_data as $k => $p_data ): ?>
						<div role="tabpanel" class="tab-pane <?php if( $count == 0 ) echo esc_attr('active'); ?>" id="<?php echo esc_attr( $k ); ?>">
							<div class="row"> <?php echo balanceTags( implode("\n", $p_data ) ); ?> </div>
						</div>
					<?php $count++;
					endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	
	<!-- Modal -->
	<div class="modal fade" id="myModaltest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?php esc_html_e('Start Discussion with Disqus', 'comre');?></h4>
				</div>
				<div class="modal-body">
					<?php echo sh_set($meta, 'coupon_code');?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close', 'comre');?></button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.get_coupon_code').each(function(index, element) {
				var elem_id = $(this).attr('id');
				var coupon_code = $(this).data('text');
				
				if( coupon_code ) {
					$("#"+elem_id).zclip({
						path: '<?php echo get_template_directory_uri(); ?>/js/ZeroClipboard.swf',
						copy: coupon_code,
						afterCopy: function() {
						   console.log('copied');
						  alert('Data in clipboard! Now you can paste it somewhere');
						}
					});
				}
			});
			
		});
	</script>
    
</section>

