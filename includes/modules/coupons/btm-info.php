<?php
/**
 * Bottom info of great deals shortcode
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
} ?>

<ul class="btm-info">

	<?php if ( sh_set( $post_meta, 'verified' ) ) : ?>
		<li class="col-xs-4"> <i class="fa fa-check-square-o"></i><?php esc_html_e( ' Verified', 'comre' );?></li>
	<?php endif;?>

	<?php if ( sh_set( $post_meta, 'safe' ) ) :?>
		<li class="col-xs-3"><a href="javascript;" class="add_to_wishlist" data-id="<?php the_ID(); ?>"> <i class="fa fa-bookmark"></i><?php esc_html_e( ' Save', 'comre' );?></a></li>
	<?php endif;?>

	<?php if ( sh_set( $post_meta, 'share' ) ) : ?>
		<li class="col-xs-2"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <i class="fa fa-share"></i><?php esc_html_e( ' Share', 'comre' );?></a></li>
	<?php endif;?>

	<?php if ( sh_set( $post_meta, 'discuss' ) ) : ?>
		<li class="col-xs-3"><a class="disqus" href="javascript;" data-id="<?php the_ID(); ?>" data-toggle="modal" data-target="#myModaltest"> <i class="fa fa-comments"></i><?php esc_html_e( ' Discuss', 'comre' );?></a></li>
	<?php endif;?>
</ul>
