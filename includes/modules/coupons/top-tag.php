<?php
/**
 * Top tag of grid coupons listing
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
} ?>

<div class="top-tag"> 

	<?php if ( in_array( 'labels', $ext ) ) :?>

		<span class="ribn-red">
			<span>

				<?php echo wp_kses_post( sh_set( $post_meta, 'banner' ) );?>
					
			</span>
		</span>

	<?php endif;?> 

	<span class="star"><i class="fa fa-star-o"></i></span>

</div>
