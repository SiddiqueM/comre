<?php if(!defined('ABSPATH')) die('Restricted Access'); ?>


<?php global $post; ?>


	<div class="<?php  echo $offers_columns?> margin-bottom">
			<div class="offer-in"> 
			<?php the_post_thumbnail('270x185', array('class' => 'img-responsive'));?>
			 
				
				<!--======= SMALL IMAGE =========--> 
				<span class="small-spon">
					<img src="<?php echo sh_set($meta, 'small_image');?>" alt="" class="img-responsive" />
				</span>
				<h6><?php the_title();?></h6>
				<p class="text-uppercase"><?php esc_html_e('Expires: ', 'comre');?><?php echo sh_set($meta, 'expires_date');?></p>
				<p><?php the_terms($post->ID, 'coupons_store_category', __('Stores: ', 'comre'), ', '); ?></p>
				
				<div class="btm-offer">
					<p class="text-uppercase"><?php echo sh_set($meta, 'cashback');?></p>
				</div>
				<!--======= SMALL IMAGE HOVER =========-->
				<h6><?php the_title();?></h6>
				<p class="text-uppercase"><?php esc_html_e('Expires: ', 'comre');?><?php echo sh_set($meta, 'expires_date');?></p>
				<p><?php the_terms($post->ID, 'coupons_store_category', __('Stores: ', 'comre'), ', '); ?></p>
				
				<div class="btm-offer">
					<p class="text-uppercase"><?php echo sh_set($meta, 'cashback');?></p>
				</div>
				<!--======= SMALL IMAGE HOVER =========-->
				<div class="off-over">
					<h6><?php the_title();?></h6>
					<p class="text-uppercase"><?php esc_html_e('Expires: ', 'comre');?><?php echo sh_set($meta, 'expires_date');?></p>
					<p><?php the_terms($post->ID, 'coupons_store_category', __('Stores:', 'comre'), ', '); ?></p>
					
					

					<?php if(sh_set( $meta, 'coupon_code')):
						$coupon_link = _set_refferer_to_link( sh_set( $meta, 'coupon_link') );
						$ext_link =  ($coupon_link) ? ' data-href="'. $coupon_link.'" target="_blank"' : ''; ?>
					
                    	<?php if(sh_set($meta, 'coupon_display_type') == 'coupon_copied'):?>
						<div class="text-center" data-id="get_the_coupon_code<?php echo $count; ?>"> 
							<a<?php echo $ext_link; ?> data-text="<?php echo sh_set($post_meta, 'coupon_code');?>" class="btn get_coupon_code" id="get_coupon_code<?php echo $count; ?>"><?php esc_html_e('get coupon code', 'comre');?></a> 
						</div>
						<?php else:?>
							<div class="text-center"> 
								<a<?php echo $ext_link; ?> class="btn get_coupon_code" data-toggle="modal" data-target="#myModal<?php echo $count;?>" id="get_coupon_code<?php echo $count; ?>"><?php esc_html_e('get coupon code', 'comre');?></a> 
							</div>
						<?php endif;?>
                    
					<?php endif;?>
					
					<div class="btm-offer">
						<p class="text-uppercase"><?php echo sh_set($meta, 'cashback');?></p>
						
						<!--======= SHARE INFO =========-->
						<ul class="btm-info">
							<?php if(sh_set($meta, 'safe')):?><li class="col-xs-4"><a href="#."> <i class="fa fa-bookmark"></i><?php esc_html_e(' Save', 'comre');?></a></li><?php endif;?>
							<?php if(sh_set($meta, 'share')):?><li class="col-xs-4"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><i class="fa fa-share"></i><?php esc_html_e(' Share ', 'comre');?></a></li><?php endif;?>
							<?php if(sh_set($meta, 'discuss')):?><li class="col-xs-4"><a class="disqus" href="javascript:;" data-id="<?php the_ID(); ?>" data-toggle="modal" data-target="#myModaltest"> <i class="fa fa-comments"></i><?php esc_html_e(' Discuss', 'comre');?></a></li><?php endif;?>
						</ul>
					</div>
				</div>
				
				<!-- Modal -->
				<div class="modal fade" id="myModal<?php echo $count;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><?php esc_html_e('Get Coupon Code', 'comre');?></h4>
					  </div>
					  <div class="modal-body">
						<?php echo sh_set($meta, 'coupon_code');?>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close', 'comre');?></button>
					  </div>
					</div>
				  </div>
				</div>
				
			</div>
		</div>
		