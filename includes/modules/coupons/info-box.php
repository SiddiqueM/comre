<?php
/**
 * Great Deals info box
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
} ?>

<?php $coupon_data = sh_set( $post_meta, 'expires_date' ); ?>

<a class="head" href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>

<?php if ( $coupon_data ) : ?>
	<p><?php esc_html_e( 'Expires On :','comre' );?> <?php echo wp_kses_post( date( get_option('date_format'), strtotime( $coupon_data ) ) );?></p>
<?php endif; ?>

<p><?php the_terms( get_the_id(), 'coupons_store_category', esc_html__( 'Stores: ', 'comre' ), ', ' ); ?></p>

<?php // Using custom function to include templates. It can be override from Child Theme.
// By placing coupon_tabs.php in child-theme/includes/modules/coupons/.
_WSH()->template_part( 'includes/modules/coupons/great_deals_coupon_button', '', compact( 'post_meta', 'ext', 'count', 'is_purchaseable' ) ); ?>
