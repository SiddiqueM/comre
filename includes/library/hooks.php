<?php
/**
 * Main hooks file
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

add_action( 'comre_single_page_header_banner', 'comre_single_page_header_banner', 10 );
add_action( 'comre_single_page_sidebar', 'comre_single_page_sidebar', 10, 3 );
add_action( 'comre_great_deals_top_tag', 'comre_great_deals_top_tag', 10, 2 );
add_action( 'comre_great_deals_post_content', 'comre_great_deals_post_content', 10, 1 );
add_action( 'comre_great_deals_info_box', 'comre_great_deals_info_box', 10, 4 );
add_action( 'comre_great_deals_btm_info', 'comre_great_deals_btm_info', 10, 2 );
add_action( 'comre_theme_initiate_hook', 'comre_theme_initiate_memory_limit' );

/**
 * Hook up with comre_single_page_header_banner to show header banner on single pages
 *
 * @return void [description]
 */
function comre_single_page_header_banner() {
	get_template_part( 'templates/header/banner', 'single' );
}

/**
 * Hooked up with comre_single_page_sidebar for all post types detail page
 *
 * @param  string $position [description].
 * @return void           [description]
 */
function comre_single_page_sidebar( $position ) {

	$meta = _WSH()->get_meta( '_sh_layout_settings' );

	$layout = sh_set( $meta, 'layout', 'full' );

	if ( ! $layout || 'full' === $layout ) {
		$sidebar = '';
	} else {
		$sidebar = sh_set( $meta, 'sidebar', 'product-sidebar' );
	}

	if ( $position === $layout && is_active_sidebar( $sidebar ) ) : ?>
		
		<li class="col-sm-3">
			<div id="sidebar" class="blog-side-bar clearfix">
				<?php dynamic_sidebar( $sidebar ); ?>
			</div>
		</li>

	<?php endif;
}

/**
 * [comre_great_deals_top_tag description]
 *
 * @param  array $ext       Array of extras passed to shortcode.
 * @param  array $post_meta Coupon post meta.
 * @return void             [description]
 */
function comre_great_deals_top_tag( $ext, $post_meta ) {

	$file = locate_template( 'includes/modules/coupons/top-tag.php' );

	if ( file_exists( $file ) ) {
		require $file;
	}
}

/**
 * [comre_great_deals_post_content description]
 *
 * @param  string $size [description].
 * @return void       [description]
 */
function comre_great_deals_post_content( $size ) {

	$file = locate_template( 'includes/modules/coupons/coupon-content.php' );

	if ( file_exists( $file ) ) {
		require $file;
	}
}

/**
 * [comre_great_deals_info_box description]
 *
 * @param  array   $post_meta [description].
 * @param  array   $ext       [description].
 * @param  integer $count     [description].
 * @return void             [description]
 */
function comre_great_deals_info_box( $post_meta, $ext, $count, $is_purchaseable ) {

	$file = locate_template( 'includes/modules/coupons/info-box.php' );

	if ( file_exists( $file ) ) {
		require $file;
	}
}

/**
 * [comre_great_deals_btm_info description]
 *
 * @param array $post_meta [description].
 * @param array $ext       [description].
 * @return void            [description]
 */
function comre_great_deals_btm_info( $post_meta, $ext ) {

	$file = locate_template( 'includes/modules/coupons/btm-info.php' );

	if ( file_exists( $file ) ) {
		require $file;
	}
}



function comre_theme_initiate_memory_limit() {

	set_time_limit( 300 );
	ini_set( 'memory_limit', '256M' );
}

