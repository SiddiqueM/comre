<?php global $product, $post;

$options = _WSH()->option();//printr($options);
$coupon_image = sh_set($options, 'coupon_image');
$btn_like = sh_set($options, 'btn_like');
//wp_enqueue_script( array( 'jquery-flexslider' ) );
get_header(); 


$meta = _WSH()->get_meta( '_sh_layout_settings' );

$layout = sh_set( $meta, 'layout', 'full' );

if ( ! $layout || 'full' === $layout ) {
	$sidebar = '';
} else {
	$sidebar = sh_set( $meta, 'sidebar', 'product-sidebar' );
}

$classes = ( ! $layout || 'full' === $layout ) ? '  col-sm-12' : ' col-sm-9'; ?>

<!--======= BANNER =========-->



<section class="blog coupon-detail-page woocommerce">
	<div class="container">

		<?php if( function_exists('WC')) wc_print_notices(); ?>

		<ul class="row">
			
			<?php
				/**
				 * Hooked up with comre_single_page_sidebar function /library/hooks.php
				 * You can hookup yours own
				 */
				do_action( 'comre_single_page_sidebar', 'left' );
			?>
			<!-- end sidebar -->
			<li <?php post_class( $classes ); ?>>
				<?php $count = 1;
				
				while( have_posts() ): the_post(); 

					$post_meta = _WSH()->get_meta(); //print_r($post_meta);

					if(sh_set( $post_meta, 'is_purchaseable') && function_exists('WC')) {
						
						$product = new WC_Product( get_the_id() );
						$product->product_type = ($product->get_type()) ? $product->get_type() : 'simple';
					}  
				       $buton_title = (sh_set($post_meta, 'buttons_title')) ? sh_set($post_meta, 'buttons_title') : 'get coupon code'; 
						$cash_back = (sh_set($post_meta, 'cashback')) ? sh_set($post_meta, 'cashback') : esc_html__('No Cashback', 'comre');
						$thumb_style = sh_set($options, 'like_style');
						$thumb_style = ( $thumb_style ) ? $thumb_style : 'style1';
				   ?>
					<div class="row">
						<div class="col-md-4">
							
							<?php echo comre_wow_themes_coupon_featured_image('830x390'); ?>
							
							<h4 class="text-center cash-back"><?php echo $cash_back; ?></h4>
							
							<h6 class="text-center"><?php the_terms($post->ID, 'coupons_store_category', __('Stores: ', 'comre'), ', '); ?></h6>
							
							<?php if(!sh_set( $post_meta, 'is_purchaseable') ): ?>
								
								
								<?php _WSH()->template_part('single_coupons/coupon_button', '', array('post_meta'=>$post_meta,  'count' => $count, 'buton_title' => $buton_title )); ?>
								
								
								<?php get_template_part( 'single_coupons/coupon_modal' ); ?>
							<?php endif; ?>

							<?php if (!sh_set($options, 'hide_social')  ): ?>

								<div class="share-post text-center">
									<h5 class="text-uppercase"><?php esc_html_e('share this post', 'comre');?></h5>
									<ul class="social_icons">
										<li class="facebook"><a href="javascript:void(0);" title="Facebook"><i class="fa fa-facebook st_facebook_large"></i></a></li>
										<li class="twitter"><a href="javascript:void(0);" title="Twitter"><i class="fa fa-twitter st_twitter_large"></i></a></li>
										<li class="linkedin"><a href="javascript:void(0);" title="Linkedin"><i class="fa fa-linkedin st_linkedin_large"></i></a></li>
										<li class="tumblr"><a href="javascript:void(0);" title="Tumblr"><i class="fa fa-tumblr st_tumblr_large"></i></a></li>
									</ul>
									<script type="text/javascript">var switchTo5x=true;</script> 
									<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script> 
									<script type="text/javascript">stLight.options({publisher: "e5f231e9-4404-49b7-bc55-0e8351a047cc", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script> 
								</div>
							<?php endif; ?>

						</div>
						
						<!--======= BLOG POST=========-->
						<div class="col-md-8">
							
							<div class="blog-post">
								
								<h2 class="heading clearfix">
								
									<a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>" class="title-hed col-md-6">
										<?php the_title(); ?>
									</a>
                                    
                                    <?php if (sh_set($options,'btn_like')):?>
									
									<?php get_template_part( 'single_coupons/like', $thumb_style ); ?>
                                    <?php endif; ?>

								</h2>
								
								
								<?php if(sh_set( $post_meta, 'is_purchaseable') && function_exists('WC')): ?>
									<?php woocommerce_template_loop_price();?>
								<?php endif; ?>
							
								<?php the_content();?>
								
								<?php if(sh_set( $post_meta, 'is_purchaseable') && function_exists('WC')): ?>
									<?php woocommerce_template_single_add_to_cart(); ?>
								<?php endif; ?>
								
								
							
							</div>
						</div>
						
					</div>
					<!--======= SHARE POST =========-->
				
					<!-- end button -->
					<div class="clearfix"></div>
					
					<!--=======  COMMENTS =========-->
					<?php $post_id = get_the_id();
					$post_url = get_permalink($post_id); ?>
					<?php if(!sh_set($options,'disqus_thread')):?>
					<div id="disqus_thread"></div>
				<?php endif;?>

				    <script type="text/javascript">
					    /* * * CONFIGURATION VARIABLES: THIS CODE IS ONLY AN EXAMPLE * * */
					    var disqus_shortname = '<?php echo (sh_set( $options, 'disqus_idetifier')) ? esc_js(sh_set( $options, 'disqus_idetifier') ) : esc_js('example'); ?>'; // Required - Replace example with your forum shortname
					    var disqus_identifier = 'disqus_thread<?php echo $post_id; ?>';
					    var disqus_title = '<?php echo get_the_title(); ?>';
					    var disqus_url = '<?php echo esc_url($post_url); ?>';

					    /* * * DON'T EDIT BELOW THIS LINE * * */
					    (function() {
					        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					    })();
					</script>
			
				<?php endwhile;?>
			</li>

			<?php
				/**
				 * Hooked up with comre_single_page_sidebar function /library/hooks.php
				 * You can hookup yours own
				 */
				do_action( 'comre_single_page_sidebar', 'right', $layout, $sidebar );
			?>
		
		</ul>
	</div>
</section>
<?php get_footer(); ?>
