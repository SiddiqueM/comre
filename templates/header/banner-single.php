<?php
/**
 * Header banner for detail templates
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

$meta1 = _WSH()->get_meta( '_sh_header_settings' );
$bg = sh_set( $meta1, 'bg_image' );
$title = sh_set( $meta1, 'header_title' ); ?>


<section class="sub-banner" <?php if ( $bg ) : ?>style="background-image: url('<?php echo esc_url( $bg ); ?>');"<?php endif;?>>
	<div class="overlay">
		<div class="container">
			<h2>
				<?php echo ( $title ) ? wp_kses_post( $title ) : wp_title( '&raquo;', false );?>
			</h2>
			<?php echo get_the_breadcrumb();?>
		</div>
	</div>
</section>
