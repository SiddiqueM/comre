*** Changelog - Comre WordPress Theme ***


2016.10.31 -- version 2.4.2
------------------------------------
1) Include Typography: Added Typogrphy Settings.
2) Sidebar Issue : Sidebar issue on coupon detail page 
3) Coupon is updated.
4) Store Page Menu is Fixed.
5) Responsive Issues are Fixed.
6) Revolution Slider is updated.
7) JS composer is Updated.

2016.10.14 -- version 2.4.1
------------------------------------
1) Frontend adding stores: isses are fixed.
2) Frontend adding coupons: issues are fixed.
3) Missing translation issues are fixed.

2016.10.06 -- version 2.4.0
------------------------------------
1) Added option to open referrer link in same window
2) Added option to open popup on coupon detail page on page load
3) Added hooks in blog detail page for developers
4) Added hook in "Great Deals" shortcode for developers
5) Added template parts for child theme compatibility.

2016.09.05 -- version 2.3.9
------------------------------------
1) Fixed styling issues on woocommerce cart page.
2) Pagination issue is fixed on category page.
3) Responsive issues are fixed in shortcode "Deals of the week"
4) Compatible with WordPress 4.6

2016.08.16 -- version 2.3.8
------------------------------------
1) great deals double tab open issue on coupon pop up and copied (fixed)
2) on store category save , share and discuss button not work (fixed)
3) on product shortcode price is static (fixed)
4) Compatible with latest version of woocommerce.

2016.07.22 -- version 2.3.7
------------------------------------
1) Latest version of Visual composer included and compatible.
2) latest version of revslider is included.
3) Popup style of coupons is changed.
4) Shop button and Newsletter option is added on popup.
5) Get Coupon Code button issue on coupon and store category pages.[Fixed]
6) External referral link issue on coupon detail issue.[Fixed]
7) Expire Coupon issue on store page.[Fixed]
8) Deal and Coupon expired button is added on coupon detail page for customers.
9) Empty expired date permalink issue.[Fixed]
10) Static Google map on contact page issue.[Fixed]

2016.05.27 -- version 2.3.6
------------------------------------
1) Point / Reward feature is added in custom account page.
2) Show/Hide "Like" buttons option is added in Theme Options.
3) Styles of "Like" buttons is added in Theme Options.

2016.05.12 -- version 2.3.5
------------------------------------
1) Recent blog posts issues.[Fixed]
2) Woocommerce orders option is added in custom account page.
3) URL claoking option is added.
4) Point / Reward feature is added.
5) Custom field option is added in Account Manager.

2016.05.09 -- version 2.3.4
------------------------------------
1) While editing "Stores" add new row button was not functional ( Fixed )
2) In Great Deals shortcode list style was not functional ( Fixed )
3) Great deal shortcode approach to show coupon is popup is changed to ajax.

2016.04.29 -- version 2.3.3
------------------------------------
1) vc_row.php issues are fixed
2) Submit coupon shortcode, added more security to restrict the spamming.
3) On homepage 2 Popular coupons PHP errors and other issues are fixed
4) Disqus popup issues are fixed
5) Coupon detail page styling issues are fixed
6) "Products" shortcode issues are fixed
7) Subscription form in footer issues are fixed.
8) Added more fields in woocommerce product for compatibility.
9) Improved self explainatory help for Coupons post type. 


2016.04.25 -- version 2.3.2
------------------------------------
1) Visual Composer 4.11.2.1 for WordPress 4.5 Compatibility.
2) updated /vc_templates/vc_row.php to place the shortcodes in "container" class
3) Fixed the issue to force tgm to update the Visual Composer.


2016.04.11 -- version 2.3.1
------------------------------------
1) Latest version of Visual composer included and compatible with Wordpress 4.5.

2016.04.11 -- version 2.3.1
------------------------------------
1) Latest version of Visual composer included and compatible.
2) latest version of revslider is included.
3) Visual Composer compatiblity issues are fixed
4) PHP notices issues are fixed.
5) Woocommerce compatibility issues are fixed.
6) Store and coupon category issues in admin are fixed.


2016.03.16 -- version 2.3.0
------------------------------------
1) Latest version of Visual composer included and compatible.
2) latest version of revslider is included.
3) added translation in theme support plugin.
4) PHP notices issues are fixed.
5) Styling issues are fixed.
6) compatible with latest version of woocommerce.

2016.02.18 -- version 2.0.2
------------------------------------
1) Store upload option is added in account page for users.
2) Cashbak text and store website link option is added on store page.
3) Default coupon image option from category, store or custom is added in Theme Options.

2016.01.22 -- version 2.0.1
------------------------------------
1) Latest version of Visual Composer is included.
2) Latest version of Revolution Slider is included.
3) Thumbs Up and Thumbs Down feature is added in coupons.
4) Store Category page is added.
5) Cashback option is added in coupons.
6) Add new coupon and add new post option is added on Account page.
7) Default coupon image option is added in Theme Options.
8) User Profile image, Name and Description is added on Account page.
9) csv import expired date issue is fixed.

2016.01.22 -- version 2.0
------------------------------------
1) Latest version of Visual Composer is included.
2) Latest version of Revolution Slider is included.
3) Added new Feature: Accounts Managment.
4) Added new shortcode "Login Form".
5) Compatible with latest version of woocommerce.
6) csv import expired date issue is fixed.

2015.12.31 -- version 1.9.3
------------------------------------
1) Compatible with latest version of Visual Composer.
2) Compatible with latest version of Revolution Slider.
3) Mobile Menu issue.[Fixed]
4) Coupon category image upload issue.[Fixed]

2015.11.19 -- version 1.9.2
------------------------------------
1) Compatible with latest version 4.8.1 of Visual Composer.
2) Mail Chimp is added in footer subscribe form.
3) Contact Form 7 support is added.
4) DisQus issues.[Fixed]
5) Small VC issues.[Fixed]
6) Theme Check issues.[Fixed]

2015.10.30 -- version 1.9.1
------------------------------------
1) Compatible with latest version 4.8 of Visual Composer.
2) Compatible with latest version 5.1 of Revolution Slider.
3) Added load more option in Coupon shortcode.

2015.10.06 -- version 1.9
------------------------------------
1) Compatible with latest version 4.7.4 of Visual Composer
2) Compatible with latest version of Woocommerce
3) Coupons code issue on Coupon detail page and coupon category page is fixed
4) Top bar login and logout issue is fixed
5) Added coupons permalink settings in theme options
6) only register users can submit new coupon

2015.08.31 -- version 1.8.1
------------------------------------
1) PHP issue in theme_support_plugin is fixed

2015.08.31 -- version 1.8
------------------------------------
1) Added feature to upload bulk coupon by uploading csv file
2) Added option to set default referrer for all coupons from theme options
3) On registraton the welcome email issue is fixed
4) Added compatibility with WP Social login plugin to connect with social networks
5) Add page Template "wishlist" to show the coupons wishlist
6) Add option in theme option whether store shortcode display woocommerce cat or coupons cat
7) In great deals shortcode on hover the whole box yellow issue is fixed.
8) Added more columns in coupons post type.
9) Auto theme update is functional now.

2015.08.05 -- version 1.7
------------------------------------
1) PHP error is fixed

2015.07.30 -- version 1.6
------------------------------------
1) Woocommerce is updated
2) RTL support
3) PHP errors are fixed

2015.07.28 -- version 1.5
------------------------------------
1) Unlimited color schemes added 
2) Sorter on stores page is functional now.
3) Coupon expiration is added
4) home2 tabs disqus and share are functional now

2015.06.29 -- version 1.4
------------------------------------
1) Added latest version of Tgm-plugin-activation
2) Lastest version of Visual composer is included.

2015.05.18 -- version 1.3
------------------------------------
1) Added the option to copy coupon code to clipboard.
2) Added the option to open referer link at the same time.

2015.05.15 -- version 1.2
------------------------------------
1) Issues in theme options are fixed
2) Coupon options ( popup / external links ) are working fine now
3) Added fields for small images in metabox
4) In woocommerce products the cashback tag is working fine now.
5) 404 page styling issues are fixed
6) banner image on some inner pages is functional now
7) Checkout and Cart page styling issues? are fixed
8) Responsive issues are fixed

2015.05.10 -- version 1.1.2
----------------------------
1) Subscribe issue is fixed
2) Disqus added
3) Save button is functional
4) Coupon detail page included
5) Coupon category page included
6) Styling issues are fixed


2015.05.07 - version 1.0
	* First release!