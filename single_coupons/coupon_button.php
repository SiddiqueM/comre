<?php if( !defined('ABSPATH') ) die('Restricted Access'); ?>

<?php $options = _WSH()->option();
$ext_link = sh_set( $post_meta, 'coupon_link' ) ? ' data-href="'.sh_set( $post_meta, 'coupon_link' ).'" target="_blank"' : '';
$copied = sh_set( $options, 'copy_coupon_code_clipboard', false );
$popup = sh_set( $options, 'show_popup_detail_page', false );
$popup = ( $popup ) ? 'true' : 'false'; ?>

<?php if ( ! sh_set( $options,'hide_coupon_btn' ) ) :?>

	<?php if ( ! $copied ) :?>
		
		<div class="text-center"> 
			<a<?php echo wp_kses_post( $ext_link ); ?> onClick="ShowCouponCodeInModal(<?php echo esc_attr( get_the_id() ); ?>, '#myModal<?php the_ID();?>')" href="#" class="btn get_coupon_code" data-toggle="modal" data-target="#myModal<?php the_ID();?>" data-popup="<?php echo esc_attr( $popup ); ?>"><?php echo esc_attr( $buton_title );?></a> 
		</div>

	<?php else:?>
			<div class="text-center"> <a<?php echo $ext_link; ?> class="btn get_coupon_code" data-text="<?php echo sh_set($post_meta, 'coupon_code');?>" id="get_coupon_code<?php the_id(); ?>" data-popup="<?php echo esc_attr( $popup ); ?>"><?php echo $buton_title;?></a> </div>
	<?php endif;?>

<?php endif;?>
