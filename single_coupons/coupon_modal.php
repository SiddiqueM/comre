<?php if( !defined('ABSPATH') ) die('Restricted Access'); ?>

<?php $options = _WSH()->option();//printr($options);
$post_meta = _WSH()->get_meta(); //print_r($post_meta);

$news_title = sh_set($options, 'news_title');
$news_form_url = sh_set($options, 'news_form_url');

$coupon_popup_title = (sh_set($options, 'coupon_popup')== 'custom') ? $options['custom_title'] : get_the_title() ; ?>

<!-- Modal -->
<div class="modal fade" id="myModal<?php the_ID();?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $coupon_popup_title;?></h4>
			</div>
			<div class="modal-body">
				<i class="fa fa-spinner fa-spin fa-lg col-md-push-6 modal-processing"></i>
			</div>
			<div class="modal-footer">
			
				<?php if(sh_set($options, 'btn_newsletter')):
					the_widget( 'SH_NewsLetter', array('title' => $news_title, 'mc' => $news_form_url, 'txt' => '') );
				endif; ?>
				
				<!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?php //esc_html_e('Close', 'comre');?></button> -->
			</div>
		</div>
	</div>
</div>