<?php if( !defined('ABSPATH') ) die('Restricted Access');
$options = _WSH()->option(); 
 ?>

<ul class="small-tag no-margin coupon-voting col-md-6">
	<li class="voting-results"><span class="error"></span><span class="save-success"></span></li>
	<li> 
		<span>
			<a onclick="ComrecouponVote(<?php the_ID(); ?>, 1, this);" title="Thumbs up" ><i class="fa fa-thumbs-o-up"></i></a>
			<a onclick="ComrecouponVote(<?php the_ID(); ?>, -1, this);" title="Thumbs Down" ><i class="fa fa-thumbs-o-down"></i></a>
			
			<?php $thumbup = (int)get_post_meta(get_the_id(), '_comre_coupon_thumbs_up', true ); 
			$thumbdown = (int)get_post_meta(get_the_id(), '_comre_coupon_thumbs_down', true ); 
			$percent = ( ($thumbdown + $thumbup) == 0 ) ? 0 : ( $thumbup / ( $thumbdown + $thumbup ) ) * 100 ; 
			$percent = !$percent ? esc_html__('No Voting Yet', 'comre') : sprintf( __('<label>%s</label>%s Success', 'comre' ), number_format((int)$percent, 2 ), '%' ); ?>
			<span class="voting-thumb-up like-style1"><?php echo balanceTags( $percent ); ?></span>
			<!--<span class="voting-thumb-down"><?php //echo (int)get_post_meta(get_the_id(), '_comre_coupon_thumbs_down', true ); ?><label><?php //esc_html_e('% Lost', 'comre'); ?> </label></span> -->
			
		</span>
		
		<?php if(sh_set( $options, 'btn_expired' )): ?>
			<a onclick="ComreMarkExpired(<?php the_ID(); ?>);" class="expire-coupon" title="<?php esc_html_e('Mark this as expired.', 'comre'); ?>"><i class="fa fa-ban" aria-hidden="true"></i></a>
		<?php endif; ?>
												
	</li>
</ul>