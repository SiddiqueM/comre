<?php
/**
 * Blog detail template
 *
 * @package Comre
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 2.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

$options = _WSH()->option();

get_header();

$settings  = sh_set( sh_set( get_post_meta( get_the_ID(), 'sh_page_meta', true ) , 'sh_page_options' ) , 0 );

$meta = _WSH()->get_meta( '_sh_layout_settings' );




$layout = sh_set( $meta, 'layout', 'full' );

if ( ! $layout || 'full' === $layout ) {
	$sidebar = '';
} else {
	$sidebar = sh_set( $meta, 'sidebar', 'product-sidebar' );
}

$classes = ( ! $layout || 'full' === $layout ) ? '  col-sm-12' : ' col-sm-9';

/** Update the post views counter */
_WSH()->post_views( true ); ?>

<!--======= BANNER =========-->

<?php
	/**
	 * Hooked up in /includes/library/hooks.php with the following
	 * comre_single_page_header_banner 10
	 */
	do_action( 'comre_single_page_header_banner' );
?>


<section class="blog">

	<div class="container">

		<ul class="row">
			
			<?php
				/**
				 * Hooked up with comre_single_page_sidebar function /library/hooks.php
				 * You can hookup yours own
				 */
				do_action( 'comre_single_page_sidebar', 'left' );
			?>
			
			<!-- end sidebar -->
			<li <?php post_class( $classes ); ?>>

				<?php
					/**
					 * Hookup the things after while loop
					 */
					do_action( 'comre_before_single_page_loop' );
				?>

				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php
						/**
						 * Hookup the things before post content
						 */
						do_action( 'comre_before_single_page_content' );
					?>

					<?php get_template_part( 'includes/modules/templates/single', get_post_format() ); ?>

					<?php
						/**
						 * Hookup the things after post content
						 */
						do_action( 'comre_after_single_page_content' );
					?>

					<?php get_template_part( 'includes/modules/templates/social-media' ); ?>

					<!-- end button -->
					<div class="clearfix"></div>
					
					<?php wp_link_pages( array( 'before' => '<div class="paginate-links">'.__( 'Pages: ', 'comre' ), 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					
					<?php get_template_part( 'includes/modules/templates/authorbox' ); ?>
	
					<?php get_template_part( 'includes/modules/templates/navigations' ); ?>

					<!--=======  COMMENTS =========-->
					<?php comments_template(); ?>
			
				<?php endwhile;?>

				<?php
					/**
					 * Hookup the things after while loop
					 */
					do_action( 'comre_after_single_page_loop' );
				?>

			</li>
			
			
			<?php
				/**
				 * Hooked up with comre_single_page_sidebar function /library/hooks.php
				 * You can hookup yours own
				 */
				do_action( 'comre_single_page_sidebar', 'right', $layout, $sidebar );
			?>

		</ul>

	</div>

</section>

<?php get_footer(); ?>
