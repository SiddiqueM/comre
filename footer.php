<?php $options = _WSH()->option();

//printr($options);
$footer_bg = sh_set( $options, 'footer_background_img' );
$show_top_footer = sh_set( $options, 'toggle_footer_top' );
$show_bottom_footer = sh_set( $options, 'toggle_footer_bottom' );
$footer_bg = ( $footer_bg ) ? ' style="background-image:url('.esc_url($footer_bg).');"' : '';
?>
<?php if(sh_set($options, 'callout'))dynamic_sidebar('footer-top-sidebar'); ?>
<!--======= FOOTER =========-->
      <?php if(($show_top_footer) || ($show_bottom_footer) ): ?>
        <footer<?php echo $footer_bg; ?>>
          <?php if($show_top_footer): ?>   
              <div class="container">
        
                 
                    <ul class="row">
                      <?php dynamic_sidebar('footer-sidebar'); ?>
                    </ul>
        
              </div>
          <?php endif; ?>
          <?php if($show_bottom_footer): ?>  

              <div class="rights">
                <?php if(sh_set($options, 'copy_right')):?>
                <p><?php echo balanceTags(sh_set($options, 'copy_right'));?></p>
                <?php endif;?>
                <?php if(sh_set($options, 'show_social_icons')):?>
                <ul class="social_icons">
                  <?php echo sh_get_social_icons(); ?>
                </ul>
                <?php endif;?>
              </div>
          <?php endif; ?>
        </footer>
      <?php endif; ?>  
	</div>
	
	<?php wp_footer(); ?>

</body>
</html>