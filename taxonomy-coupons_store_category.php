<?php global $wp_query;
$options = _WSH()->option();
$coupon_image = sh_set($options, 'coupon_image');
//wp_enqueue_script( array( 'jquery-flexslider' ) );
get_header(); 

$object = get_queried_object();//printr($object);
$meta2 = _WSH()->get_meta();

//_WSH()->page_settings = $meta;
$layout = sh_set( $options, 'category_page_layout', 'full' );
if( !$layout || $layout == 'full' || sh_set($_GET, 'layout_style')=='full' ) $sidebar = ''; else
$sidebar = sh_set( $options, 'category_page_sidebar', 'product-sidebar' );
$classes = ( !$layout || $layout == 'full' || sh_set($_GET, 'layout_style')=='full' ) ? ' col-md-12 col-sm-12 col-xs-12' : ' col-md-8 col-sm-12 col-xs-12';
/** Update the post views counter */
_WSH()->post_views( true );
$bg = sh_set( $options, 'category_page_header_img' );
$title = sh_set($object, 'category_page_name');
?>


 <section class="sub-banner <?php if($bg):?>style="background-image: url('<?php echo esc_url($bg); ?>');"<?php endif;?>">
    <div class="overlay">
      <div class="container">
        <h2><?php if($title) echo  balanceTags( $title ); else wp_title('');?></h2>
        <?php echo get_the_breadcrumb();?>
      </div>
    </div>
  </section>

<section class="store-category no-padding-bottom">
	
    <div class="container">

        <div class="row">
    		<div class="col-md-2">

                <?php if( $store_img ): ?>
        			<img src="<?php echo esc_url( $store_img ); ?>" class="img-responsive" />
                <?php else: ?>
                    <img src="http://placehold.it/160x160.png" alt="">
                <?php endif; ?>
    		
            </div>
    		<div class="col-md-9">
    			<h2><?php echo sh_set($object, 'name');?></h2>
    			<p><?php echo sh_set($object, 'description'); ?></p>
    		</div>
        </div>
	</div>

	<div class="container">
		
        <div class="row">
            <h4 class="cash-back"><?php echo $cash_back; ?></h4>
    		
            <?php if($web_link): ?>
    		  <a href="<?php echo $web_link; ?>" class="btn text-center" target="_blank"><?php esc_html_e('View Store', 'comre'); ?></a>
    		<?php endif; ?>
        </div>

    
      <table class="table">
        <thead>
          <tr>
            <th><?php esc_html_e('Orders', 'comre' ); ?></th>
            <th><?php esc_html_e('Cashback', 'comre' ); ?></th>
          </tr>
        </thead>
        <tbody>
        <?php foreach((array)$cash_table as $cash_tab):
          $cash_label = sh_set($cash_tab, 'label');
          $cash_value = sh_set($cash_tab, 'value');
        ?>
          <tr>
            <td><?php echo $cash_label; ?></td>
            <td><?php echo $cash_value; ?></td>
          </tr>
        <?php endforeach; ?>
           
        </tbody>
      </table>
    
	</div>
</section>

<section class="great-deals" id="portfoli">
    <div class="container">
		<div class="tittle"> 
        	<h2><?php esc_html_e('Store Coupons', 'comre');?></h2>
		</div>

        <div class="coupon">
            
            <?php if(have_posts()):  

                $count = 0;?>
            
                <ul class="row">
                    
                    <!--======= COUPEN DEALS =========-->
                     <?php while(have_posts()): the_post();

                        global $post ; 
                        $post_meta = _WSH()->get_meta();?>

                        <?php $buton_title = (sh_set($post_meta, 'buttons_title')) ? sh_set($post_meta, 'buttons_title') : 'get coupon code';   ?>

                        <li class="col-md-4 col-sm-6 col-xs-12">
                            
                            <div class="coupon-inner">
                                <div class="top-tag">
                                    <?php  if (sh_set($post_meta, 'banner') ) :?>
                                        <span class="ribn-red"><span><?php echo sh_set($post_meta, 'banner');?></span></span> 
                                    <?php endif; ?>
                                    <span class="star"><i class="fa fa-star-o"></i></span> 
                                </div>
                                <div class="c-img">
        							
        							<?php echo comre_wow_themes_coupon_featured_image('324x143'); ?>
                                
        							<a class="head" href="<?php the_permalink();?>"><?php the_title(); ?></a>

                                    <?php if( sh_set($post_meta, 'expires_date') ): ?>
                                        
                                        <p><?php esc_html_e('Expires On :','comre');?> <?php echo sh_set($post_meta, 'expires_date');?></p>
                                    
                                    <?php endif;
        							
                                    $cash_back = (sh_set($post_meta, 'cashback')) ? sh_set($post_meta, 'cashback') : esc_html__('No Cashback', 'comre'); ?>
        							
        							<h4 class="text-center cash-back"><?php echo $cash_back; ?></h4>
                                    
                                    <?php $ext_link =  sh_set($post_meta, 'coupon_link') ? ' data-href="'. sh_set($post_meta, 'coupon_link').'" target="_blank"' : ''; ?>
                                   
                                    <?php if(sh_set($post_meta, 'coupon_display_type') == 'coupon_popup'):?>
            						
                                        <div class="text-center"> 
											<a<?php echo $ext_link; ?> class="btn get_coupon_code" data-toggle="modal" data-target="#myModalCouponCode" id="get_coupon_code<?php echo $count; ?>" onClick="ShowCouponCodeInModal(<?php echo get_the_id(); ?>, '#myModalCouponCode', this)"><?php echo $buton_title;?></a> 
										</div>
            						
                                    <?php else:?>
        								<div class="text-center" data-id="get_the_coupon_code<?php echo $count; ?>"> 
											<a<?php echo $ext_link; ?> data-text="<?php echo sh_set($post_meta, 'coupon_code');?>" class="btn get_coupon_code" id="get_coupon_code<?php echo $count; ?>"><?php echo $buton_title;?></a> 
										</div>
            						<?php endif;?>
                                    
                                    
                                </div>
                                
                            <ul class="btm-info">
                                <?php if(sh_set($post_meta, 'verified')):?><li class="col-xs-4"> <i class="fa fa-check-square-o"></i><?php esc_html_e(' Verified', 'comre');?></li><?php endif;?>
                                <?php if(sh_set($post_meta, 'safe')):?><li class="col-xs-3"><a href="javascript;" class="add_to_wishlist" data-id="<?php the_ID(); ?>"> <i class="fa fa-bookmark"></i><?php esc_html_e(' Save', 'comre');?></a></li><?php endif;?>
                                <?php if(sh_set($post_meta, 'share')):?><li class="col-xs-2"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <i class="fa fa-share"></i><?php esc_html_e(' Share', 'comre');?></a></li><?php endif;?>
                                <?php if(sh_set($post_meta, 'discuss')):?><li class="col-xs-3"><a class="disqus" href="javascript;" data-id="<?php the_ID(); ?>" data-toggle="modal" data-target="#myModaltest"> <i class="fa fa-comments"></i><?php esc_html_e(' Discuss', 'comre');?></a></li><?php endif;?>
                        </ul>
                            </div>
                        </li>
    					
    					<?php $coupon_popup_title = (sh_set($options, 'coupon_popup')== 'custom') ? $options['custom_title'] : get_the_title() ;
							$news_title = sh_set($options, 'news_title');
							$news_form_url = sh_set($options, 'news_form_url'); ?>
                        
                        <!-- Modal -->
                       <div class="modal fade" id="myModalCouponCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php echo $coupon_popup_title;?></h4>
						
									</div>
									<div class="modal-body">
										<i class="fa fa-spinner fa-spin fa-lg col-md-push-6 modal-processing"></i>
									</div>
									<div class="modal-footer">
									
										<?php if(sh_set($options, 'btn_newsletter')):
											the_widget( 'SH_NewsLetter', array('title' => $news_title, 'mc' => $news_form_url, 'txt' => '') );
										endif; ?>
										
										<!--<button type="button" class="btn btn-default" data-dismiss="modal"><?php //esc_html_e('Close', 'comre');?></button> -->
									</div>
								</div>
							</div>
						</div>
                        <div class="modal fade" id="myModaltest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title"><?php esc_html_e('Start Discussion with Disqus', 'comre');?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php echo sh_set($post_meta, 'coupon_code');?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close', 'comre');?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $count++; 

                    endwhile; //End while loop ?>
                    
                    <!--======= COUPEN DEALS =========-->
                    
                </ul>
            
            <?php else: ?>
                
                <p class="text-center"><?php esc_html_e('Sorry! No coupon found in this store. Try searching the coupons from the top search box', 'comre'); ?> </p>
            <?php endif?>

        </div>
        
    </div>

</section>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.get_coupon_code').each(function(index, element) {

			var elem_id = $(this).attr('id');
			var coupon_code = $(this).data('text');
			
			if( coupon_code ) {
				$("#"+elem_id).zclip({
					path: '<?php echo get_template_directory_uri(); ?>/js/ZeroClipboard.swf',
					copy: coupon_code,
					afterCopy: function() {
					   console.log('copied');
					  alert('Data in clipboard! Now you can paste it somewhere');
					}
				});
			}
		});
		
	});
</script>

<?php wp_enqueue_script(array('zeroclipboard'));
get_footer(); ?>